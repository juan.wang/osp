import React from "react";
import { App } from "../widget/app/App";
import { ClientOptions, connect, EnvType } from "osp-client-js";

const _env = process.env.NEXT_PUBLIC_API_ENV as EnvType;

const env = {
  api_env: _env,
};

const options: ClientOptions = {
  urlOverride: {
    api:
      typeof location !== "undefined"
        ? location?.origin + `/${env.api_env}_api/v2`
        : "",
    imageUrl: "https://test.trexprotocol.com",
  },
  timeout: 10000,
  browser: true,
  version: "",
  env: env.api_env, //'dev' or 'beta'
  error: (error) => {
    console.log(error);
  },
};

export default function Example() {
  const [client, setClient] = React.useState();
  React.useEffect(() => {
    const client = connect("key", "", "app_id", options);
    console.log("client", client.address, "sadasdasd");
    setClient(client);
  }, []);
  if (!client) return <div>loading...</div>;
  return <App client={client} />;
}
