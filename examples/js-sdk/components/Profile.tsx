import React from "react";
import { useState } from "react";
import { Client, FollowModuleEnum, User as UserType } from "osp-client-js";

export const Profile = ({ client }: { client: Client }) => {
  const [form, setForm] = useState({
    follow_module: FollowModuleEnum.NULL,
    follow_nft_recived: "",
    follow_nft_currency: "",
    follow_nft_amount: "",
  });
  const [profileInfo, setProfileInfo] = useState({});
  const [profileId, setProfileId] = useState("");

  const [profileList, setProfileList] = useState<UserType[]>([]);
  const [recommendProfileList, setRecommendProfileList] = useState([]);
  const [asset, setAsset] = useState<UserType[]>([]);

  const createProfiles = async () => {
    console.log(form, "formform");
    const {
      follow_module,
      handle,
      follow_nft_amount,
      follow_nft_recived,
      ...rest
    } = form;

    const res = await client.profile.create(
      {
        handle,
        follow_module: {
          type: follow_module,
          init:
            follow_module === FollowModuleEnum.NULL
              ? {}
              : {
                  FeeFollowModule: {
                    amount: {
                      currency: follow_nft_currency,
                      value: follow_nft_amount,
                    },
                    recipient: follow_nft_recived,
                  },
                },
        },
      },
      undefined,
      (id) => {
        console.log("success" + id);
      }
    );
    console.log("res", res);
  };
  const getAllProfiles = async () => {
    console.log(client.wallet.account);
    const { data: list } = await client.profile.getAllProfile({
      address: client.wallet.account.address,
    });
    if (!list) return;
    setProfileList(list);
    setProfileId(list[0]?.profile_id);
  };

  const getAllRecommendProfiles = async () => {
    const { data: list } = await client.profile.getAllProfile({
      ranking: "RECOMMEND",
      community_id: "0x9",
      next_token: "",
      limit: 20,
    });
    if (!list) return;
    setRecommendProfileList(list);
  };

  const getProfile = async (profileId) => {
    const data = await client.profile.get(profileId);
    setProfileInfo(data);
  };
  const getProfileSBT = async (tokenId) => {
    const data = await client.nft.getProfileSBT(tokenId);
    setProfileInfo(data);
  };

  const getByHandle = async () => {
    const {
      follow_module,
      handle,
      follow_nft_amount,
      follow_nft_recived,
      ...rest
    } = form;
    const data = await client.profile.getByHandle(handle);
    console.log("data", data);
    setProfileInfo(data);
  };

  const getAsset = async (profileId) => {
    const data = await client.profile.getAsset(profileId, {
      // "type": "NFT",
      type: "SBT",
      limit: 20,
      next_token: "",
    });
    setAsset(data);
  };

  const update = async () => {
    const profileId = client.profile.profileId;
    const res = await client.profile.update(profileId, {
      intro: "gggggg11",
      background: "adasd11",
    });
    console.log("ppppp", res);
  };

  const formChange = (key, value) => setForm({ ...form, [key]: value });

  const typeChange = (e) => {
    if (!(e.target.value === FollowModuleEnum.FeeFollowModule)) {
      formChange("follow_nft_currency", undefined);
      formChange("follow_nft_amount", undefined);
    }
    formChange("follow_module", e.target.value);
  };

  return (
    <div>
      <h2>Profile模块</h2>
      <h3>创建用户的profile</h3>
      类型:
      <select onChange={typeChange} value={form.follow_module}>
        {/*                <option value="FeeFollowModule">付费关注</option>
                <option value="ProfileFollowModule">已有profile</option>
                <option value="RevertFollowModule">拒接关注</option>*/}
        <option value="NullFollowModule">空</option>
      </select>
      {form.follow_module === FollowModuleEnum.FeeFollowModule && (
        <>
          <br />
          币种:{" "}
          <input
            onChange={(e) => formChange("follow_nft_currency", e.target.value)}
          />
          <br />
          数量:{" "}
          <input
            onChange={(e) => formChange("follow_nft_amount", e.target.value)}
          />
          <br />
          接受地址:{" "}
          <input
            onChange={(e) => formChange("follow_nft_recived", e.target.value)}
          />
        </>
      )}
      <br />
      handle: <input onChange={(e) => formChange("handle", e.target.value)} />
      <br />
      <button onClick={createProfiles}>创建用户的profile</button>
      <button onClick={getByHandle}>通过handle获取用户的profile</button>
      <br />
      <h3>获取热门用户</h3>
      <button onClick={getAllRecommendProfiles}>获取热门用户</button>
      {recommendProfileList.map((i) => (
        <>
          <br />
          <span>{i.profile_id}</span>
          <button onClick={() => getProfile(i.profile_id)}>
            获取用户的profile详情
          </button>

          <button onClick={() => getAsset(i.profile_id)}>
            获取用户的NFT和SBT
          </button>
        </>
      ))}
      <br />
      <h3>获取用户所有的profile</h3>
      <button onClick={getAllProfiles}>获取用户的所有profile</button>
      {profileList.map((i) => (
        <>
          <br />
          <span>{i.profile_id}</span>
          <button onClick={() => getProfile(i.profile_id)}>
            获取用户的profile详情
          </button>

          <button onClick={() => getAsset(i.profile_id)}>
            获取用户的NFT和SBT
          </button>
          <button onClick={() => getProfileSBT(i.profile_id)}>
            获取用户的Profile SBT
          </button>
        </>
      ))}
      <h3>更新用户信息</h3>
      <button onClick={update}>更新用户</button>
    </div>
  );
};
