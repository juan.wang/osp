import React from "react";
import { Client } from "osp-client-js";

export function CoinMintPlugin({ client }: { client: Client }) {
  const createCoin = async () => {
    const res = await client.coin_mint_plugin.createCoinOnChain(
      {
        //onChainData
        name: "name",
        symbol: "SYMBOL",
        decimals: 18,
        initSupply: 1000_000000000000000000n,
        claimSupply: 100_000000000000000000n,
        claimLimitPreAddress: 1_000000000000000000n,
        claimStartTime: 0,
        claimEndTime: Math.ceil(new Date().getTime() / 1000) + 600,
        claimPriceRange: [1, 5, 10],
        claimPrice: [
          1_000000000000000000n,
          2_000000000000000000n,
          3_000000000000000000n,
        ],
        claimPayCurrencyAddress: "0x0000000000000000000000000000000000000000",
        //   claimPayCurrencyAddress:"0xc9e3b01e153c83cbf88ea46061ce67b9593588d0",
        communityIds: [10000082],
      },
      (tokenAddress, res) => {
        console.log("tokenAddress:" + tokenAddress);
        console.log(res);
      }
    );
    console.log(res);
  };

  const updateCoin = async () => {
    const res = await client.coin_mint_plugin.updateCoinOffChain(
      "79679995287207936",
      {
        remark: "remark",
        banner: "banner",
        distribution: "distribution",
      }
    );
    console.log(res);
  };

  const claimCoin = async () => {
    const res = await client.coin_mint_plugin.claimCoinOnChain(
      "0xA9403C879EFFBd1171051F923E47C2160291ED61",
      10000082,
      100000000000,
      () => {
        console.log("claim success");
      }
    );
    console.log(res);
  };

  return (
    <>
      <h2>CoinMint插件</h2>
      <button onClick={createCoin}>链上创建token</button>
      <button onClick={updateCoin}>更新token数据</button>
      <button onClick={claimCoin}>领取token</button>
    </>
  );
}
