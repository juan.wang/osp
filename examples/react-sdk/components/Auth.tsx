import React from "react";
import { useLogout, useLogin } from "react-client-js";
import { WALLET_TYPE } from "osp-client-js";

export function Auth() {
  const {
    execute: loginIn,
    isPending: isLogining,
    error: loginInError,
  } = useLogin();
  const {
    execute: loginOut,
    isPending: isLogouting,
    error: logoutError,
  } = useLogout();
  const handleSignOut = async () => {
    await loginOut();
  };
  const handleSignIn = async (type) => {
    const res = await loginIn(type);
    console.log(res);
  };

  return (
    <>
      <h2>Auth模块</h2>

      <h3>登录</h3>

      <button onClick={() => handleSignIn(WALLET_TYPE.METAMASK)}>
        metamask signIn
      </button>
      <button onClick={() => handleSignIn(WALLET_TYPE.GOOGLE)}>
        google signIn
      </button>
      <button onClick={() => handleSignIn(WALLET_TYPE.FACEBOOK)}>
        facebook signIn
      </button>
      <button onClick={() => handleSignIn(WALLET_TYPE.TWITTER)}>
        twitter signIn
      </button>
      <h3>登出</h3>
      <button onClick={() => handleSignOut()}>logout</button>
    </>
  );
}
