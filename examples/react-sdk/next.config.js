module.exports = {
  reactStrictMode: true,
  transpilePackages: ["ui"],
  async rewrites() {
    return [
      {
        source: '/dev_api/:path*',
        destination: 'https://dev.opensocial.trex.xyz/:path*',
      },
      {
        source: '/beta_api/:path*',
        destination: 'https://opensocial.trex.xyz/:path*',
      },
      {
        source: '/local_api/:path*',
        destination: 'http://172.18.1.211:8081/:path*',
      }
    ]
  },


};
