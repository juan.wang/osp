import dynamic from "next/dynamic";
import React from "react";

const Auth = dynamic(
  () => import("../../components/Auth").then((T) => T.Auth),
  {
    ssr: false,
  }
);

export function App(props) {
  return (
    <div>
      <h1>JS SDK Example：</h1>
      <Auth {...props} />
    </div>
  );
}
