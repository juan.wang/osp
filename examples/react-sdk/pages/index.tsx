import React from "react";
import { App } from "../widget/app/App";
import { EnvType } from "osp-client-js";
import { OspProvider } from "react-client-js";
const _env = process.env.NEXT_PUBLIC_API_ENV as EnvType;

const env = {
  api_env: _env,
};

const options = {
  urlOverride: {
    api:
      typeof location !== "undefined"
        ? location?.origin + `/${env.api_env}_api/v2`
        : "",
    imageUrl: "https://test.trexprotocol.com",
  },
  timeout: 10000,
  browser: true,
  version: "",
  env: env.api_env, //'dev' or 'beta'
  error: (error) => {
    console.log(error);
  },
};

export default function Example() {
  // const {initClient}  = useClient()
  // const { execute: loginIn, error, isPending } = useWalletLogin()
  // const client = initClient(options)
  // console.log('client', client)
  // loginIn(WALLET_TYPE.METAMASK)

  return (
    <OspProvider options={options}>
      <App />
    </OspProvider>
  );

  // if (!client) return <div>loading...</div>;
  // return <App client={client} />;
}
