import { useSharedDependencies } from "../shared";
import { WALLET_TYPE, OspRes } from "osp-client-js";
import { useOperation } from "../helpers/operations";

/**
 * `useLogin` is a auth loginIn hook that lets you loginIn via web2 or web3
 *
 *
 * @category Auth
 * @group Hooks
 * @param args - {@link }
 *
 * @example
 * ```tsx
  import React from "react";
  import { useLogin } from "react-client-js";
  import { WALLET_TYPE } from "osp-client-js";

  export function Auth() {
    const {
      execute: loginIn,
      isPending: isLogining,
      error: loginInError,
    } = useLogin();

    const handleSignIn = async (type) => {
      const res = await loginIn(type);
      console.log(res);
    };

    return (
      <>
        <h2>Auth模块</h2>

        <h3>登录</h3>

        <button onClick={() => handleSignIn(WALLET_TYPE.METAMASK)}>
          metamask signIn
        </button>
        <button onClick={() => handleSignIn(WALLET_TYPE.GOOGLE)}>
          google signIn
        </button>
        <button onClick={() => handleSignIn(WALLET_TYPE.FACEBOOK)}>
          facebook signIn
        </button>
        <button onClick={() => handleSignIn(WALLET_TYPE.TWITTER)}>
          twitter signIn
        </button>

      </>
    );
  }
 * ```
 */
export function useLogin() {
  const { client } = useSharedDependencies();

  return useOperation(async (walletType: WALLET_TYPE): Promise<OspRes> => {
    return client.auth.signIn(walletType);
  });
}

/**
 * `useLogout` is a auth loginOut hook that lets you loginOut
 *
 *
 * @category Auth
 * @group Hooks
 * @param args - {@link }
 *
 * @example
 * ```tsx
  import React from "react";
  import { useLogout } from "react-client-js";

  export function Auth() {
    const {
      execute: loginOut,
      isPending: isLogouting,
      error: logoutError,
    } = useLogout();

    const handleSignOut = async () => {
      await loginOut();
    };

    return (
      <>
        <h2>Auth模块</h2>
        <h3>登出</h3>
        <button onClick={() => handleSignOut()}>logout</button>
      </>
    );
  }
 * ```
 */

export function useLogout() {
  const { client } = useSharedDependencies();
  return useOperation(async (): Promise<void> => {
    return client.auth.signOut();
  });
}
