export type Cast<A, B> = A extends B ? A : B;
/**
 * Primitive types
 * @internal
 */
export type Primitive =
  | string
  | number
  | boolean
  | bigint
  | symbol
  | undefined
  | null;

export type Narrow<A> = Cast<
  A,
  [] | (A extends Primitive ? A : never) | { [K in keyof A]: Narrow<A[K]> }
>;
class Success<T, E> {
  /** @internal */
  public constructor(public readonly value: T) {}

  public isSuccess(): this is Success<T, E> {
    return true;
  }

  public isFailure(): this is Failure<T, E> {
    return false;
  }

  unwrap(): T {
    return this.value;
  }
}

/**
 * A `Success<T, E>` represents a successful computation that returns a value of type `T`.
 *
 * `T` in `Failure<T, E>` is the type of the value that would have been returned in case of success.
 * It's present only to allow type safety of the `isSuccess` method.
 *
 * @sealed
 * @privateRemarks DO NOT EXPORT, see type export later on
 */
class Failure<T, E> {
  /** @internal */
  public constructor(public readonly error: E) {}

  public isSuccess(): this is Success<T, E> {
    return false;
  }

  public isFailure(): this is Failure<T, E> {
    return true;
  }

  unwrap(): never {
    throw this.error;
  }
}

export type { Success, Failure };
/**
 * An `IEquatableError` is an error that can be compared by name.
 *
 */
export interface IEquatableError<T extends string = string, P = Narrow<T>> {
  name: P;
}

export type Result<T, E extends IEquatableError> =
  | Success<T, E>
  | Failure<T, E>;

/**
 * A `PromiseResult` is a convenience type alias that represents either a {@link Result} in the context of asynchronous tasks.
 */
export type PromiseResult<T, E extends IEquatableError> = Promise<Result<T, E>>;
