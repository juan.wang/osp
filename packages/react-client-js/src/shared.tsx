import { ReactNode, useContext, createContext } from "react";
import { invariant } from "./helpers/invariant";
import { Client, DefaultGenerics, ClientOptions, connect } from "osp-client-js";

export type SharedDependencies = {
  client: Client<DefaultGenerics>;
};
type SharedDependenciesProviderProps = {
  children: ReactNode;
  dependencies: SharedDependencies;
};

const SharedDependenciesContext = createContext<SharedDependencies | null>(
  null
);
export function createSharedDependencies(
  options: ClientOptions
): SharedDependencies {
  const client = connect("key", "", "app_id", options);
  return {
    client,
  };
}
export function SharedDependenciesProvider({
  children,
  dependencies: context,
}: SharedDependenciesProviderProps) {
  return (
    <SharedDependenciesContext.Provider value={context}>
      {children}
    </SharedDependenciesContext.Provider>
  );
}

export function useSharedDependencies(): SharedDependencies {
  const context: SharedDependencies = useContext(SharedDependenciesContext);

  invariant(
    context,
    "Could not find  SDK context, ensure your code is wrapped in a <OspProvider>"
  );

  return context;
}
