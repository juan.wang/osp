import { ReactNode, useEffect, useRef, useState } from "react";
import { createSharedDependencies, SharedDependenciesProvider } from "./shared";
import { ClientOptions } from "osp-client-js";

/**
 * <OspProvider> props
 */
export type OspProviderProps = {
  /**
   * The children to render
   */
  children: ReactNode;
  /**
   * The configuration for the  SDK
   */
  options: ClientOptions;
};

// A specific function type would not trigger implicit any.
// See https://github.com/DefinitelyTyped/DefinitelyTyped/issues/52873#issuecomment-845806435 for a comparison between `Function` and more specific types.
// eslint-disable-next-line
function useLatestCallback<T extends Function>(callback: T) {
  const latestCallbackRef = useRef(callback);

  useEffect(() => {
    latestCallbackRef.current = callback;
  }, [callback]);

  return (...args: unknown[]) =>
    latestCallbackRef.current.apply(null, args) as T;
}

/**
 * Manages the lifecycle and internal state of the  SDK
 *
 * @group Components
 * @param props - {@link OspProviderProps}
 */
export function OspProvider({ children, ...props }: OspProviderProps) {
  const [sharedDependencies] = useState(() => {
    return createSharedDependencies(props.options);
  });

  const isStartedRef = useRef(false);

  useEffect(() => {
    // Protects again multiple calls to start (quite likely from `useEffect` hook in concurrent mode (or in strict mode))
    if (isStartedRef.current) {
      return;
    }

    isStartedRef.current = true;
  }, []);

  return (
    <SharedDependenciesProvider dependencies={sharedDependencies}>
      {children}
    </SharedDependenciesProvider>
  );
}
