# react-client-js@0.0.1

react-client-js is a osp clent js sdk.

## Installation

Use the package manager [pnpm] or [npm] to install osp-client-js.

```bash

pnpm add react-client-js
```

or

```bash
npm i react-client-js --save-dev
```

## Usage

```python

```

# Changelog

## [0.0.1]

### Change

- release react client js sdk

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
