/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */
import { Signer, utils, Contract, ContractFactory, Overrides } from "ethers";
import type { Provider, TransactionRequest } from "@ethersproject/providers";
import type { PromiseOrValue } from "../../../../common";
import type {
  SlotNFTCondition,
  SlotNFTConditionInterface,
} from "../../../../contracts/core/conditions/SlotNFTCondition";

const _abi = [
  {
    inputs: [
      {
        internalType: "address",
        name: "osp",
        type: "address",
      },
    ],
    stateMutability: "nonpayable",
    type: "constructor",
  },
  {
    inputs: [],
    name: "InitParamsInvalid",
    type: "error",
  },
  {
    inputs: [],
    name: "NotGovernance",
    type: "error",
  },
  {
    inputs: [],
    name: "NotOSP",
    type: "error",
  },
  {
    inputs: [],
    name: "NotSlotNFTOwner",
    type: "error",
  },
  {
    inputs: [],
    name: "SlotNFTAlreadyUsed",
    type: "error",
  },
  {
    inputs: [],
    name: "SlotNFTNotWhitelisted",
    type: "error",
  },
  {
    inputs: [],
    name: "OSP",
    outputs: [
      {
        internalType: "address",
        name: "",
        type: "address",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "address",
        name: "to",
        type: "address",
      },
      {
        internalType: "bytes",
        name: "data",
        type: "bytes",
      },
    ],
    name: "condition",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "address",
        name: "slot",
        type: "address",
      },
    ],
    name: "isCommunitySlotWhitelisted",
    outputs: [
      {
        internalType: "bool",
        name: "",
        type: "bool",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "address",
        name: "slot",
        type: "address",
      },
      {
        internalType: "address",
        name: "addr",
        type: "address",
      },
      {
        internalType: "uint256",
        name: "tokenId",
        type: "uint256",
      },
    ],
    name: "isSlotNFTUsable",
    outputs: [
      {
        internalType: "bool",
        name: "",
        type: "bool",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "address",
        name: "slot",
        type: "address",
      },
      {
        internalType: "bool",
        name: "whitelist",
        type: "bool",
      },
    ],
    name: "whitelistCommunitySlot",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
] as const;

const _bytecode =
  "0x60a060405234801561001057600080fd5b5060405161073338038061073383398101604081905261002f916100a0565b806001600160a01b038116610057576040516348be0eb360e01b815260040160405180910390fd5b6001600160a01b03811660808190526040514281527ff1a1fa6b64aa95186f5a1285e76198d0da80d9c5a88062641d447f1d7c54e56c9060200160405180910390a250506100d0565b6000602082840312156100b257600080fd5b81516001600160a01b03811681146100c957600080fd5b9392505050565b60805161063b6100f86000396000818160890152818161012201526101b1015261063b6000f3fe608060405234801561001057600080fd5b50600436106100575760003560e01c80632d204e241461005c57806341990ea3146100715780639f9854e514610084578063ac8b0233146100c8578063f57aaecf14610104575b600080fd5b61006f61006a366004610494565b610117565b005b61006f61007f366004610519565b6101af565b6100ab7f000000000000000000000000000000000000000000000000000000000000000081565b6040516001600160a01b0390911681526020015b60405180910390f35b6100f46100d6366004610557565b6001600160a01b031660009081526001602052604090205460ff1690565b60405190151581526020016100bf565b6100f461011236600461057b565b61028d565b336001600160a01b037f0000000000000000000000000000000000000000000000000000000000000000161461016057604051632ae096a960e11b815260040160405180910390fd5b60008061016f838501856105bc565b9150915061017e85838361035f565b6001600160a01b0390911660009081526020818152604080832093835292905220805460ff19166001179055505050565b7f00000000000000000000000000000000000000000000000000000000000000006001600160a01b031663289b3c0d6040518163ffffffff1660e01b8152600401602060405180830381865afa15801561020d573d6000803e3d6000fd5b505050506040513d601f19601f8201168201806040525081019061023191906105e8565b6001600160a01b0316336001600160a01b03161461026257604051632d5be4cb60e21b815260040160405180910390fd5b6001600160a01b03919091166000908152600160205260409020805460ff1916911515919091179055565b6001600160a01b03831660009081526001602052604081205460ff1680156102d757506001600160a01b03841660009081526020818152604080832085845290915290205460ff16155b801561035757506040516331a9108f60e11b8152600481018390526001600160a01b038085169190861690636352211e90602401602060405180830381865afa158015610328573d6000803e3d6000fd5b505050506040513d601f19601f8201168201806040525081019061034c91906105e8565b6001600160a01b0316145b949350505050565b6001600160a01b03821660009081526001602052604081205460ff161515900361039b576040516283c69360e21b815260040160405180910390fd5b6040516331a9108f60e11b8152600481018290526001600160a01b038085169190841690636352211e90602401602060405180830381865afa1580156103e5573d6000803e3d6000fd5b505050506040513d601f19601f8201168201806040525081019061040991906105e8565b6001600160a01b03161461043057604051637f59164160e01b815260040160405180910390fd5b6001600160a01b03821660009081526020818152604080832084845290915290205460ff16151560010361047757604051630da293d160e01b815260040160405180910390fd5b505050565b6001600160a01b038116811461049157600080fd5b50565b6000806000604084860312156104a957600080fd5b83356104b48161047c565b9250602084013567ffffffffffffffff808211156104d157600080fd5b818601915086601f8301126104e557600080fd5b8135818111156104f457600080fd5b87602082850101111561050657600080fd5b6020830194508093505050509250925092565b6000806040838503121561052c57600080fd5b82356105378161047c565b91506020830135801515811461054c57600080fd5b809150509250929050565b60006020828403121561056957600080fd5b81356105748161047c565b9392505050565b60008060006060848603121561059057600080fd5b833561059b8161047c565b925060208401356105ab8161047c565b929592945050506040919091013590565b600080604083850312156105cf57600080fd5b82356105da8161047c565b946020939093013593505050565b6000602082840312156105fa57600080fd5b81516105748161047c56fea264697066735822122054421bfb258a7e512bf504f9c9290386b1542e05d3d74588c51eef09a6f6dc0564736f6c63430008120033";

type SlotNFTConditionConstructorParams =
  | [signer?: Signer]
  | ConstructorParameters<typeof ContractFactory>;

const isSuperArgs = (
  xs: SlotNFTConditionConstructorParams
): xs is ConstructorParameters<typeof ContractFactory> => xs.length > 1;

export class SlotNFTCondition__factory extends ContractFactory {
  constructor(...args: SlotNFTConditionConstructorParams) {
    if (isSuperArgs(args)) {
      super(...args);
    } else {
      super(_abi, _bytecode, args[0]);
    }
  }

  override deploy(
    osp: PromiseOrValue<string>,
    overrides?: Overrides & { from?: PromiseOrValue<string> }
  ): Promise<SlotNFTCondition> {
    return super.deploy(osp, overrides || {}) as Promise<SlotNFTCondition>;
  }
  override getDeployTransaction(
    osp: PromiseOrValue<string>,
    overrides?: Overrides & { from?: PromiseOrValue<string> }
  ): TransactionRequest {
    return super.getDeployTransaction(osp, overrides || {});
  }
  override attach(address: string): SlotNFTCondition {
    return super.attach(address) as SlotNFTCondition;
  }
  override connect(signer: Signer): SlotNFTCondition__factory {
    return super.connect(signer) as SlotNFTCondition__factory;
  }

  static readonly bytecode = _bytecode;
  static readonly abi = _abi;
  static createInterface(): SlotNFTConditionInterface {
    return new utils.Interface(_abi) as SlotNFTConditionInterface;
  }
  static connect(
    address: string,
    signerOrProvider: Signer | Provider
  ): SlotNFTCondition {
    return new Contract(address, _abi, signerOrProvider) as SlotNFTCondition;
  }
}
