/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */
export { ICommunityLogic__factory } from "./ICommunityLogic__factory";
export { IGovernanceLogic__factory } from "./IGovernanceLogic__factory";
export { IPluginLogic__factory } from "./IPluginLogic__factory";
export { IProfileLogic__factory } from "./IProfileLogic__factory";
export { IPublicationLogic__factory } from "./IPublicationLogic__factory";
export { IReactionLogic__factory } from "./IReactionLogic__factory";
export { IRelationLogic__factory } from "./IRelationLogic__factory";
export { OspClient__factory } from "./OspClient__factory";
