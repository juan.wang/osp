/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */

import { ethers } from "ethers";
import {
  FactoryOptions,
  HardhatEthersHelpers as HardhatEthersHelpersBase,
} from "@nomiclabs/hardhat-ethers/types";

import * as Contracts from ".";

declare module "hardhat/types/runtime" {
  interface HardhatEthersHelpers extends HardhatEthersHelpersBase {
    getContractFactory(
      name: "Initializable",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.Initializable__factory>;
    getContractFactory(
      name: "ERC20Upgradeable",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.ERC20Upgradeable__factory>;
    getContractFactory(
      name: "IERC20MetadataUpgradeable",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IERC20MetadataUpgradeable__factory>;
    getContractFactory(
      name: "IERC20Upgradeable",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IERC20Upgradeable__factory>;
    getContractFactory(
      name: "ERC721Upgradeable",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.ERC721Upgradeable__factory>;
    getContractFactory(
      name: "ERC721EnumerableUpgradeable",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.ERC721EnumerableUpgradeable__factory>;
    getContractFactory(
      name: "IERC721EnumerableUpgradeable",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IERC721EnumerableUpgradeable__factory>;
    getContractFactory(
      name: "IERC721MetadataUpgradeable",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IERC721MetadataUpgradeable__factory>;
    getContractFactory(
      name: "IERC721ReceiverUpgradeable",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IERC721ReceiverUpgradeable__factory>;
    getContractFactory(
      name: "IERC721Upgradeable",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IERC721Upgradeable__factory>;
    getContractFactory(
      name: "ContextUpgradeable",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.ContextUpgradeable__factory>;
    getContractFactory(
      name: "ERC165Upgradeable",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.ERC165Upgradeable__factory>;
    getContractFactory(
      name: "IERC165Upgradeable",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IERC165Upgradeable__factory>;
    getContractFactory(
      name: "IERC1822Proxiable",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IERC1822Proxiable__factory>;
    getContractFactory(
      name: "IERC1967",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IERC1967__factory>;
    getContractFactory(
      name: "IBeacon",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IBeacon__factory>;
    getContractFactory(
      name: "ERC1967Proxy",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.ERC1967Proxy__factory>;
    getContractFactory(
      name: "ERC1967Upgrade",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.ERC1967Upgrade__factory>;
    getContractFactory(
      name: "Proxy",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.Proxy__factory>;
    getContractFactory(
      name: "ERC20",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.ERC20__factory>;
    getContractFactory(
      name: "IERC20Permit",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IERC20Permit__factory>;
    getContractFactory(
      name: "IERC20Metadata",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IERC20Metadata__factory>;
    getContractFactory(
      name: "IERC20",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IERC20__factory>;
    getContractFactory(
      name: "IERC721",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IERC721__factory>;
    getContractFactory(
      name: "ERC165",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.ERC165__factory>;
    getContractFactory(
      name: "IERC165",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IERC165__factory>;
    getContractFactory(
      name: "Multicall",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.Multicall__factory>;
    getContractFactory(
      name: "ERC721BurnableUpgradeable",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.ERC721BurnableUpgradeable__factory>;
    getContractFactory(
      name: "OspNFTBase",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.OspNFTBase__factory>;
    getContractFactory(
      name: "OspSBTBase",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.OspSBTBase__factory>;
    getContractFactory(
      name: "CollectNFT",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.CollectNFT__factory>;
    getContractFactory(
      name: "CommunityNFT",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.CommunityNFT__factory>;
    getContractFactory(
      name: "SlotNFTCondition",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.SlotNFTCondition__factory>;
    getContractFactory(
      name: "WhitelistAddressCondition",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.WhitelistAddressCondition__factory>;
    getContractFactory(
      name: "FollowSBT",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.FollowSBT__factory>;
    getContractFactory(
      name: "JoinNFT",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.JoinNFT__factory>;
    getContractFactory(
      name: "CommunityLogic",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.CommunityLogic__factory>;
    getContractFactory(
      name: "GovernanceLogic",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.GovernanceLogic__factory>;
    getContractFactory(
      name: "ICommunityLogic",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.ICommunityLogic__factory>;
    getContractFactory(
      name: "IGovernanceLogic",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IGovernanceLogic__factory>;
    getContractFactory(
      name: "IPluginLogic",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IPluginLogic__factory>;
    getContractFactory(
      name: "IProfileLogic",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IProfileLogic__factory>;
    getContractFactory(
      name: "IPublicationLogic",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IPublicationLogic__factory>;
    getContractFactory(
      name: "IReactionLogic",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IReactionLogic__factory>;
    getContractFactory(
      name: "IRelationLogic",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IRelationLogic__factory>;
    getContractFactory(
      name: "OspClient",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.OspClient__factory>;
    getContractFactory(
      name: "PluginLogic",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.PluginLogic__factory>;
    getContractFactory(
      name: "ProfileLogic",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.ProfileLogic__factory>;
    getContractFactory(
      name: "PublicationLogic",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.PublicationLogic__factory>;
    getContractFactory(
      name: "ReactionLogic",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.ReactionLogic__factory>;
    getContractFactory(
      name: "RelationLogic",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.RelationLogic__factory>;
    getContractFactory(
      name: "FreeCollectModule",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.FreeCollectModule__factory>;
    getContractFactory(
      name: "FeeModuleBase",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.FeeModuleBase__factory>;
    getContractFactory(
      name: "FollowValidationModuleBase",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.FollowValidationModuleBase__factory>;
    getContractFactory(
      name: "ERC20FeeJoinModule",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.ERC20FeeJoinModule__factory>;
    getContractFactory(
      name: "HoldTokenJoinModule",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.HoldTokenJoinModule__factory>;
    getContractFactory(
      name: "IToken",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IToken__factory>;
    getContractFactory(
      name: "NativeFeeJoinModule",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.NativeFeeJoinModule__factory>;
    getContractFactory(
      name: "JoinValidatorJoinModuleBase",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.JoinValidatorJoinModuleBase__factory>;
    getContractFactory(
      name: "ModuleBase",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.ModuleBase__factory>;
    getContractFactory(
      name: "FollowerOnlyReferenceModule",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.FollowerOnlyReferenceModule__factory>;
    getContractFactory(
      name: "CoinMintPlugin",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.CoinMintPlugin__factory>;
    getContractFactory(
      name: "CoinProxy",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.CoinProxy__factory>;
    getContractFactory(
      name: "ERC20Coin",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.ERC20Coin__factory>;
    getContractFactory(
      name: "PluginBase",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.PluginBase__factory>;
    getContractFactory(
      name: "ICollectModule",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.ICollectModule__factory>;
    getContractFactory(
      name: "ICollectNFT",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.ICollectNFT__factory>;
    getContractFactory(
      name: "ICommunityNFT",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.ICommunityNFT__factory>;
    getContractFactory(
      name: "ICondition",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.ICondition__factory>;
    getContractFactory(
      name: "IERC4906",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IERC4906__factory>;
    getContractFactory(
      name: "IERC721Burnable",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IERC721Burnable__factory>;
    getContractFactory(
      name: "IFollowModule",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IFollowModule__factory>;
    getContractFactory(
      name: "IFollowSBT",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IFollowSBT__factory>;
    getContractFactory(
      name: "IJoinModule",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IJoinModule__factory>;
    getContractFactory(
      name: "IJoinNFT",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IJoinNFT__factory>;
    getContractFactory(
      name: "IOspNFTBase",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IOspNFTBase__factory>;
    getContractFactory(
      name: "IReferenceModule",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IReferenceModule__factory>;
    getContractFactory(
      name: "OspErrors",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.OspErrors__factory>;
    getContractFactory(
      name: "OspEvents",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.OspEvents__factory>;
    getContractFactory(
      name: "Currency",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.Currency__factory>;
    getContractFactory(
      name: "Helper",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.Helper__factory>;
    getContractFactory(
      name: "MockCondition",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.MockCondition__factory>;
    getContractFactory(
      name: "MockFollowModule",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.MockFollowModule__factory>;
    getContractFactory(
      name: "MockNFTBase",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.MockNFTBase__factory>;
    getContractFactory(
      name: "MockReferenceModule",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.MockReferenceModule__factory>;
    getContractFactory(
      name: "FollowSBTProxy",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.FollowSBTProxy__factory>;
    getContractFactory(
      name: "IRouter",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.IRouter__factory>;
    getContractFactory(
      name: "JoinNFTProxy",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.JoinNFTProxy__factory>;
    getContractFactory(
      name: "OspRouterImmutable",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.OspRouterImmutable__factory>;
    getContractFactory(
      name: "OspUniversalProxy",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.OspUniversalProxy__factory>;
    getContractFactory(
      name: "TransparentUpgradeableProxy",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.TransparentUpgradeableProxy__factory>;

    getContractAt(
      name: "Initializable",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.Initializable>;
    getContractAt(
      name: "ERC20Upgradeable",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.ERC20Upgradeable>;
    getContractAt(
      name: "IERC20MetadataUpgradeable",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IERC20MetadataUpgradeable>;
    getContractAt(
      name: "IERC20Upgradeable",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IERC20Upgradeable>;
    getContractAt(
      name: "ERC721Upgradeable",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.ERC721Upgradeable>;
    getContractAt(
      name: "ERC721EnumerableUpgradeable",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.ERC721EnumerableUpgradeable>;
    getContractAt(
      name: "IERC721EnumerableUpgradeable",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IERC721EnumerableUpgradeable>;
    getContractAt(
      name: "IERC721MetadataUpgradeable",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IERC721MetadataUpgradeable>;
    getContractAt(
      name: "IERC721ReceiverUpgradeable",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IERC721ReceiverUpgradeable>;
    getContractAt(
      name: "IERC721Upgradeable",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IERC721Upgradeable>;
    getContractAt(
      name: "ContextUpgradeable",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.ContextUpgradeable>;
    getContractAt(
      name: "ERC165Upgradeable",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.ERC165Upgradeable>;
    getContractAt(
      name: "IERC165Upgradeable",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IERC165Upgradeable>;
    getContractAt(
      name: "IERC1822Proxiable",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IERC1822Proxiable>;
    getContractAt(
      name: "IERC1967",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IERC1967>;
    getContractAt(
      name: "IBeacon",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IBeacon>;
    getContractAt(
      name: "ERC1967Proxy",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.ERC1967Proxy>;
    getContractAt(
      name: "ERC1967Upgrade",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.ERC1967Upgrade>;
    getContractAt(
      name: "Proxy",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.Proxy>;
    getContractAt(
      name: "ERC20",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.ERC20>;
    getContractAt(
      name: "IERC20Permit",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IERC20Permit>;
    getContractAt(
      name: "IERC20Metadata",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IERC20Metadata>;
    getContractAt(
      name: "IERC20",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IERC20>;
    getContractAt(
      name: "IERC721",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IERC721>;
    getContractAt(
      name: "ERC165",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.ERC165>;
    getContractAt(
      name: "IERC165",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IERC165>;
    getContractAt(
      name: "Multicall",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.Multicall>;
    getContractAt(
      name: "ERC721BurnableUpgradeable",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.ERC721BurnableUpgradeable>;
    getContractAt(
      name: "OspNFTBase",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.OspNFTBase>;
    getContractAt(
      name: "OspSBTBase",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.OspSBTBase>;
    getContractAt(
      name: "CollectNFT",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.CollectNFT>;
    getContractAt(
      name: "CommunityNFT",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.CommunityNFT>;
    getContractAt(
      name: "SlotNFTCondition",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.SlotNFTCondition>;
    getContractAt(
      name: "WhitelistAddressCondition",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.WhitelistAddressCondition>;
    getContractAt(
      name: "FollowSBT",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.FollowSBT>;
    getContractAt(
      name: "JoinNFT",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.JoinNFT>;
    getContractAt(
      name: "CommunityLogic",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.CommunityLogic>;
    getContractAt(
      name: "GovernanceLogic",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.GovernanceLogic>;
    getContractAt(
      name: "ICommunityLogic",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.ICommunityLogic>;
    getContractAt(
      name: "IGovernanceLogic",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IGovernanceLogic>;
    getContractAt(
      name: "IPluginLogic",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IPluginLogic>;
    getContractAt(
      name: "IProfileLogic",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IProfileLogic>;
    getContractAt(
      name: "IPublicationLogic",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IPublicationLogic>;
    getContractAt(
      name: "IReactionLogic",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IReactionLogic>;
    getContractAt(
      name: "IRelationLogic",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IRelationLogic>;
    getContractAt(
      name: "OspClient",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.OspClient>;
    getContractAt(
      name: "PluginLogic",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.PluginLogic>;
    getContractAt(
      name: "ProfileLogic",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.ProfileLogic>;
    getContractAt(
      name: "PublicationLogic",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.PublicationLogic>;
    getContractAt(
      name: "ReactionLogic",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.ReactionLogic>;
    getContractAt(
      name: "RelationLogic",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.RelationLogic>;
    getContractAt(
      name: "FreeCollectModule",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.FreeCollectModule>;
    getContractAt(
      name: "FeeModuleBase",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.FeeModuleBase>;
    getContractAt(
      name: "FollowValidationModuleBase",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.FollowValidationModuleBase>;
    getContractAt(
      name: "ERC20FeeJoinModule",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.ERC20FeeJoinModule>;
    getContractAt(
      name: "HoldTokenJoinModule",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.HoldTokenJoinModule>;
    getContractAt(
      name: "IToken",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IToken>;
    getContractAt(
      name: "NativeFeeJoinModule",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.NativeFeeJoinModule>;
    getContractAt(
      name: "JoinValidatorJoinModuleBase",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.JoinValidatorJoinModuleBase>;
    getContractAt(
      name: "ModuleBase",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.ModuleBase>;
    getContractAt(
      name: "FollowerOnlyReferenceModule",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.FollowerOnlyReferenceModule>;
    getContractAt(
      name: "CoinMintPlugin",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.CoinMintPlugin>;
    getContractAt(
      name: "CoinProxy",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.CoinProxy>;
    getContractAt(
      name: "ERC20Coin",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.ERC20Coin>;
    getContractAt(
      name: "PluginBase",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.PluginBase>;
    getContractAt(
      name: "ICollectModule",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.ICollectModule>;
    getContractAt(
      name: "ICollectNFT",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.ICollectNFT>;
    getContractAt(
      name: "ICommunityNFT",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.ICommunityNFT>;
    getContractAt(
      name: "ICondition",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.ICondition>;
    getContractAt(
      name: "IERC4906",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IERC4906>;
    getContractAt(
      name: "IERC721Burnable",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IERC721Burnable>;
    getContractAt(
      name: "IFollowModule",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IFollowModule>;
    getContractAt(
      name: "IFollowSBT",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IFollowSBT>;
    getContractAt(
      name: "IJoinModule",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IJoinModule>;
    getContractAt(
      name: "IJoinNFT",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IJoinNFT>;
    getContractAt(
      name: "IOspNFTBase",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IOspNFTBase>;
    getContractAt(
      name: "IReferenceModule",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IReferenceModule>;
    getContractAt(
      name: "OspErrors",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.OspErrors>;
    getContractAt(
      name: "OspEvents",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.OspEvents>;
    getContractAt(
      name: "Currency",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.Currency>;
    getContractAt(
      name: "Helper",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.Helper>;
    getContractAt(
      name: "MockCondition",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.MockCondition>;
    getContractAt(
      name: "MockFollowModule",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.MockFollowModule>;
    getContractAt(
      name: "MockNFTBase",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.MockNFTBase>;
    getContractAt(
      name: "MockReferenceModule",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.MockReferenceModule>;
    getContractAt(
      name: "FollowSBTProxy",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.FollowSBTProxy>;
    getContractAt(
      name: "IRouter",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.IRouter>;
    getContractAt(
      name: "JoinNFTProxy",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.JoinNFTProxy>;
    getContractAt(
      name: "OspRouterImmutable",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.OspRouterImmutable>;
    getContractAt(
      name: "OspUniversalProxy",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.OspUniversalProxy>;
    getContractAt(
      name: "TransparentUpgradeableProxy",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.TransparentUpgradeableProxy>;

    // default types
    getContractFactory(
      name: string,
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<ethers.ContractFactory>;
    getContractFactory(
      abi: any[],
      bytecode: ethers.utils.BytesLike,
      signer?: ethers.Signer
    ): Promise<ethers.ContractFactory>;
    getContractAt(
      nameOrAbi: string | any[],
      address: string,
      signer?: ethers.Signer
    ): Promise<ethers.Contract>;
  }
}
