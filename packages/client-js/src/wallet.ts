import Web3 from "web3";

import {
  CustomChainConfig,
  WALLET_ADAPTERS,
  WALLET_ADAPTER_TYPE,
  getEvmChainConfig,
} from "@web3auth/base";
import { EthereumPrivateKeyProvider } from "@web3auth/ethereum-provider";
import { Web3AuthNoModal } from "@web3auth/no-modal";
import {
  OpenloginAdapter,
  OpenloginLoginParams,
} from "@web3auth/openlogin-adapter";

import { MetamaskAdapter } from "@web3auth/metamask-adapter";
import { ethers } from "ethers";
import { hexlify, toUtf8Bytes } from "ethers/lib/utils";
import { Hex, hexToNumber } from "viem";
import { AbiItem } from "web3-utils";
import { OspRes, WALLET_ERROR, clientId } from "./constant";
import { OspAccount } from "./erc4337/osp-account";
import { Request } from "./request";
import {
  ERC20FeeJoinModule,
  ERC20FeeJoinModule__factory,
  HoldTokenJoinModule,
  HoldTokenJoinModule__factory,
  NativeFeeJoinModule,
  NativeFeeJoinModule__factory,
  OspClient,
  OspClient__factory,
  SlotNFTCondition,
  SlotNFTCondition__factory,
  CoinMintPlugin,
  CoinMintPlugin__factory,
} from "typechain-types";

interface ContractAddress {
  /** osp contract address */
  osp?: string;
  /** community nft contract address */
  community_nft?: string;
  /** slot nft condition address */
  slot_nft_condition?: string;
  /** whitelist address condition address */
  whitelist_address_condition?: string;
  /** slot nft addresses */
  slot_nft?: string[];
  /** free collect module address */
  free_collect_module?: string;
  /** follower only reference module address */
  follower_only_reference_module?: string;
  /** ERC20 fee join module address */
  erc20_fee_join_module?: string;
  /** native token fee join module address */
  native_fee_join_module?: string;
  hold_token_join_module?: string;
  coin_mint_plugin_address?: string;
  entry_point_address?: string;
  account_factory_address?: string;
  paymaster_address?: string;
  chain_id?: string;
  alchemy_api?: string;
}

export class Wallet {
  request: Request;
  config: CustomChainConfig;
  web3Auth: Web3AuthNoModal;
  web3: Web3;
  eoaAddress: string;
  contractSlot: SlotNFTCondition;
  contractConfig: ContractAddress;
  etherProvider: ethers.providers.Web3Provider;
  account: OspAccount;
  ospClient: OspClient;
  holdTokenJoinModule: HoldTokenJoinModule;
  erc20FeeJoinModule: ERC20FeeJoinModule;
  nativeFeeJoinModule: NativeFeeJoinModule;
  //plugin
  coinMintPlugin: CoinMintPlugin;
  constructor(request: Request, config: CustomChainConfig) {
    this.request = request;
    this.config = config;
    this.init();
  }

  async addressAndContractInit(): Promise<OspRes> {
    this.etherProvider = new ethers.providers.Web3Provider(
      this.web3Auth.provider
    );
    this.eoaAddress = (await this.web3.eth.getAccounts())[0];
    if (!this.eoaAddress) {
      return { error: { code: WALLET_ERROR.NO_WALLET } };
    }
    const { data, error } = await this.request.call(
      ["contract", "getContractConfig"],
      {}
    );
    if (error) {
      return { error: error };
    }
    try {
      this.contractConfig = data;
      this.contractSlot = SlotNFTCondition__factory.connect(
        this.contractConfig.slot_nft_condition,
        this.etherProvider.getSigner()
      );
      this.ospClient = OspClient__factory.connect(
        this.contractConfig.osp,
        this.etherProvider.getSigner()
      );
      this.holdTokenJoinModule = HoldTokenJoinModule__factory.connect(
        this.contractConfig.hold_token_join_module,
        this.etherProvider.getSigner()
      );
      this.erc20FeeJoinModule = ERC20FeeJoinModule__factory.connect(
        this.contractConfig.erc20_fee_join_module,
        this.etherProvider.getSigner()
      );
      this.nativeFeeJoinModule = NativeFeeJoinModule__factory.connect(
        this.contractConfig.native_fee_join_module,
        this.etherProvider.getSigner()
      );
      this.coinMintPlugin = CoinMintPlugin__factory.connect(
        this.contractConfig.coin_mint_plugin_address,
        this.etherProvider.getSigner()
      );
      this.account = new OspAccount(this);
      await this.account.init();
    } catch (e) {
      return { error: { code: WALLET_ERROR.WALLET_CONTRACT_INIT_ERROR, ...e } };
    }
    return { data: this.eoaAddress };
  }

  async loadWeb3Auth(): Promise<Web3AuthNoModal> {
    // @ts-ignore
    const chainConfig = this.config;
    const web3AuthInstance = new Web3AuthNoModal({
      chainConfig,
      clientId,
      enableLogging: true,
      web3AuthNetwork: "testnet",
    });
    const openloginAdapter = new OpenloginAdapter({
      privateKeyProvider: new EthereumPrivateKeyProvider({
        config: {
          chainConfig: getEvmChainConfig(
            hexToNumber(this.config.chainId as Hex)
          ) as CustomChainConfig,
        },
      }),
    });
    web3AuthInstance.configureAdapter(openloginAdapter);

    const metamaskAdapter = new MetamaskAdapter({
      clientId,
      sessionTime: 86400,
      chainConfig: this.config,
    });
    web3AuthInstance.configureAdapter(metamaskAdapter);
    await web3AuthInstance.init();
    return web3AuthInstance;
  }

  async init() {
    const web3AuthInstance = await this.loadWeb3Auth();
    this.web3Auth = web3AuthInstance;
    if (web3AuthInstance.connected) {
      this.web3 = new Web3(web3AuthInstance.provider as any);
      await this.addressAndContractInit();
    }
  }

  getContractABI(abi: AbiItem[] | AbiItem, contractAddress: string): any {
    return new this.web3.eth.Contract(abi, contractAddress, {
      from: this.eoaAddress, // 默认交易发送地址
    });
  }

  async connect(
    walletName: WALLET_ADAPTER_TYPE = WALLET_ADAPTERS.METAMASK,
    loginParams?: OpenloginLoginParams
  ) {
    if (!this.web3Auth) {
      console.log("web3auth not initialized yet");
      return;
    }
    if (this.web3Auth.connected) {
      await this.web3Auth.logout();
    }
    const provider = await this.web3Auth.connectTo<OpenloginLoginParams>(
      walletName,
      loginParams
    );
    this.web3 = new Web3(provider as any);
    return await this.addressAndContractInit();
  }

  async logout() {
    if (!this.web3Auth || !this.web3Auth.connected) {
      console.log("web3auth not initialized yet!");
      return;
    }
    await this.web3Auth.logout();
  }

  async getUserInfo() {
    if (!this.web3Auth || !this.web3Auth.connected) {
      console.log("web3auth not initialized yet!");
      return;
    }
    return this.web3Auth.getUserInfo();
  }

  async getAccounts() {
    if (!this.web3) {
      console.log("provider not initialized yet");
      return;
    }
    return this.web3.eth.getAccounts();
  }

  async getChainId() {
    return await this.web3.eth.getChainId();
  }

  async personalSign(message: string | Uint8Array) {
    try {
      const resolvedMessage =
        typeof message === "string" ? toUtf8Bytes(message) : message;
      const result = await (this.web3.currentProvider as any)?.request({
        method: "personal_sign",
        params: [hexlify(resolvedMessage), this.eoaAddress],
        from: this.eoaAddress,
      });
      return { data: result };
    } catch (error) {
      console.log("error", error);
      // @ts-ignore
      return { error: { ...error, code: WALLET_ERROR[error.code] } };
    }
  }

  async signMessage(type_data: any) {
    try {
      const params = [this.eoaAddress, JSON.stringify(type_data)];
      const method = "eth_signTypedData_v4";
      let res = await (this.web3.currentProvider as any)?.request({
        method,
        params,
        from: this.eoaAddress,
      });
      return { data: res };
    } catch (error) {
      // @ts-ignore
      return { error: { ...error, code: WALLET_ERROR[error.code] } };
    }
  }
}
