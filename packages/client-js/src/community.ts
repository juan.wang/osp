import { ethers } from "ethers";
import { Hex } from "viem/dist/types/types/misc";
import { hexToNumber } from "web3-utils";
import { Client, DefaultGenerics } from "./client";
import { BatchUserOp } from "./erc4337/osp-account";
import { CHAIN_CONFIG, WALLET_ERROR } from "./constant";

import {
  ConditionEnum,
  JoinModule,
  JoinModuleEnum,
  SetJoinModuleRequest,
  ListCommunityRequest,
  PostTypeEnum,
} from "./rest_api_generated";
import { IERC20__factory } from "typechain-types";
import { getEvent } from "./erc4337/receipt";

export class Community<
  StreamFeedGenerics extends DefaultGenerics = DefaultGenerics
> {
  client: Client<StreamFeedGenerics>;
  data: any;
  userId: any;
  profileId: any;

  constructor(client: Client<StreamFeedGenerics>) {
    this.client = client;
    this.data = undefined;
  }

  async mintSlotNft() {
    const res = await this.client.request.call(["mint", "mintSlotNft"], {
      address: this.client.wallet.account.address,
    });
    return res;
  }

  async checkSlotNFT(tokenId: string): Promise<any> {
    try {
      const data = await this.client.wallet.contractSlot.isSlotNFTUsable(
        this.client.wallet.contractConfig.slot_nft[0],
        this.client.wallet.account.address,
        tokenId
      );
      return { error: null, data };
    } catch (e) {
      return { error: e, data: null };
    }
  }

  /**
   * Delete the user
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#removing-users
   * @return {Promise<APIResponse>}
   */
  async create(
    createData: {
      handle: string;
      tokenId: any;
      join_module?: JoinModule;
      type?: PostTypeEnum.DRAFT | PostTypeEnum.INDEX;
      tags?: string[];
    },
    onCompleted = (communityId?: string) => {}
  ) {
    //@ts-ignore
    createData.condition = {
      type: ConditionEnum.SLOT_NFT_CONDITION,
      param: {
        slot_nft_condition: {
          slot_nft_address: this.client.wallet.contractConfig?.slot_nft?.[0],
          token_id: createData?.tokenId,
        },
      },
    };
    if (createData.join_module == undefined || createData.join_module == null) {
      createData.join_module = createData.join_module || {
        type: JoinModuleEnum.NULL_JOIN_MODULE,
        init: {
          null_join_module: {},
        },
      };
    }
    const { tokenId, type, ...rest } = createData;
    // TODO 目前community的typeData还用来存储 还不能删除
    const { data, error } = await this.client.typedata.communityCreate({
      ...rest,
      type,
      token_id: tokenId,
    });
    if (error) return { error, data: null };
    if (type === "DRAFT") {
      onCompleted();
      return { data, error };
    }

    let conditionAddress = this.client.wallet.contractConfig.slot_nft_condition;
    let conditionInitData = ethers.utils.defaultAbiCoder.encode(
      ["address", "uint256"],
      [this.client.wallet.contractConfig.slot_nft[0], tokenId]
    );

    let { joinModuleAddress, joinModuleInitData } = this.getJoinModuleInitData(
      createData.join_module
    );
    let communityCallData =
      this.client.wallet.ospClient.interface.encodeFunctionData(
        "createCommunity",
        [
          {
            handle: createData.handle,
            condition: conditionAddress,
            conditionData: conditionInitData,
            joinModule: joinModuleAddress,
            joinModuleInitData: joinModuleInitData,
          },
        ]
      ) as Hex;
    return this.client.wallet.account.sendOpAndGetResult(
      { data: communityCallData },
      (receipt) => {
        onCompleted(
          ethers.utils.hexValue(
            getEvent("CommunityCreated", receipt)["communityId"]
          )
        );
      }
    );
  }

  // TODO 方法抽取
  getJoinModuleInitData(joinModule: JoinModule) {
    let joinModuleInitData = "0x";
    let joinModuleAddress = "0x0000000000000000000000000000000000000000";
    switch (joinModule.type) {
      case JoinModuleEnum.NULL_JOIN_MODULE:
        break;
      case JoinModuleEnum.ERC20FEEJOINMODULE:
        joinModuleAddress =
          this.client.wallet.contractConfig.erc20_fee_join_module;
        joinModuleInitData = ethers.utils.defaultAbiCoder.encode(
          ["address", "uint256", "address"],
          [
            joinModule.init.erc20_fee_join_module.currency_address,
            joinModule.init.erc20_fee_join_module.amount_uint,
            joinModule.init.erc20_fee_join_module.recipient_address,
          ]
        );
        break;
      case JoinModuleEnum.HOLD_TOKEN_JOIN_MODULE:
        joinModuleAddress =
          this.client.wallet.contractConfig.hold_token_join_module;
        joinModuleInitData = ethers.utils.defaultAbiCoder.encode(
          ["address", "uint256"],
          [
            joinModule.init.hold_token_join_module.token_address,
            joinModule.init.hold_token_join_module.amount_uint,
          ]
        );
        break;
      case JoinModuleEnum.NATIVE_FEE_JOIN_MODULE:
        joinModuleAddress =
          this.client.wallet.contractConfig.native_fee_join_module;
        joinModuleInitData = ethers.utils.defaultAbiCoder.encode(
          ["uint256", "address"],
          [
            joinModule.init.native_fee_join_module.amount_uint,
            joinModule.init.native_fee_join_module.recipient_address,
          ]
        );
    }
    return { joinModuleAddress, joinModuleInitData };
  }

  async setJoinModule(
    updateData: SetJoinModuleRequest,
    onCompleted = () => {}
  ) {
    let { joinModuleAddress, joinModuleInitData } = this.getJoinModuleInitData(
      updateData.join_module
    );
    let data = this.client.wallet.ospClient.interface.encodeFunctionData(
      "setJoinModule",
      [
        BigInt(hexToNumber(updateData.community_id)),
        joinModuleAddress,
        joinModuleInitData,
      ]
    ) as Hex;
    return this.client.wallet.account.sendOpAndGetResult({ data }, onCompleted);
  }

  /**
   * Delete the user
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#removing-users
   * @return {Promise<APIResponse>}
   */
  async join(joinData: { id: string }, onCompleted = (tokenId: string) => {}) {
    const { id } = joinData;
    const decId = BigInt(hexToNumber(id));
    try {
      let batchUserOp = await getJoinCallData(this.client, decId);
      return await this.client.wallet.account.sendOpAndGetResult(
        batchUserOp,
        (receipt) => {
          onCompleted(
            ethers.utils.hexValue(
              getEvent("JoinNFTTransferred", receipt)["joinNFTId"]
            )
          );
        }
      );
    } catch (e) {
      return {
        data: null,
        error: {
          code: e.message,
        },
      };
    }
  }

  /**
   * Get the user profile
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#retrieving-users
   * @param {boolean} [options.with_follow_counts]
   * @return {Promise<StreamUser>}
   */
  async get(communitId: string) {
    const res = await this.client.request.call(
      ["communities", "getCommunityById"],
      communitId
    );
    const { data, error } = res;
    let values = data;
    if (!error && data?.join_module_details?.amount) {
      const { join_module_details, ...rest } = data;
      const { amount, ...parma } = join_module_details;
      let unit = ethers.utils.formatUnits(amount, 18);
      values = {
        join_module_details: {
          amount: unit,
          ...parma,
        },
        ...rest,
      };
    }
    if (!error && values?.draft?.join_module_details?.amount) {
      const { join_module_details, ...rest } = values?.draft;
      const { amount, ...parma } = join_module_details;
      let unit = ethers.utils.formatUnits(amount, 18);
      values = {
        ...values,
        draft: {
          join_module_details: {
            amount: unit,
            ...parma,
          },
          ...rest,
        },
      };
    }
    return { data: { ...values }, error };
  }

  async getTags(communitId: string) {
    const res = await this.client.request.call(
      ["communities", "getCommunityTags"],
      communitId
    );
    return res;
  }

  /**
   * Update a new user in stream
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#adding-users
   * @param {object} data user date stored in stream
   * @param {boolean} [options.get_or_create] if user already exists return it
   * @return {Promise<StreamUser>}
   */
  async update(communitId: string, info: any, onCompleted = () => {}) {
    const res = await this.client.request.call(
      ["communities", "updateCommunityById"],
      communitId,
      info
    );
    if (res.error) {
      return res;
    }
    if (info.type === "DRAFT") {
      onCompleted();
      return res;
    }
    if (info.fields.join_module) {
      return await this.setJoinModule(
        {
          community_id: communitId,
          join_module: <JoinModule>info.fields.join_module,
        },
        onCompleted
      );
    } else {
      onCompleted();
    }
    return res;
  }

  async updateDomain(communitId: string, query: { domain: string }) {
    const res = await this.client.request.call(
      ["communities", "updateDomainById"],
      communitId,
      query
    );
    return res;
  }

  /**
   * Get the user profile
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#retrieving-users
   * @param {boolean} [options.with_follow_counts]
   * @return {Promise<StreamUser>}
   */
  async getByHandle(handle: string, is_check: boolean = false) {
    const res = await this.client.request.call(
      ["communities", "getCommunityByHandle"],
      handle,
      is_check
    );
    return res;
  }

  /**
   * Get the user all profile
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#retrieving-users
   * @param {boolean} [options.with_follow_counts]
   * @return {Promise<StreamUser>}
   */
  async getAllCommunity(query: ListCommunityRequest) {
    const res = await this.client.request.call(
      ["communities", "listCommunity"],
      query
    );
    return res;
  }

  async getAllJoin(profileId: string, query: ListCommunityRequest) {
    const _profileId = profileId || this.client.profile.profileId;
    const queryBody = {
      ...{
        with_reverse: true,
        limit: 20,
        next_token: "",
        include_target_ids: [],
        exclude_target_ids: [],
      },
      ...query,
    };

    const res = await this.client.request.call(
      ["relations", "getProfileJoining"],
      _profileId,
      queryBody
    );
    return res;
  }

  async getAllCreated(query: ListCommunityRequest) {
    console.log(this.client.profile.profileId);
    const res = await this.client.request.call(
      ["communities", "listCommunity"],
      {
        ...query,
        profile_id: this.client.profile.profileId,
      }
    );
    return res;
  }
}

export async function getJoinCallData(
  client: Client,
  communityId: bigint
): Promise<BatchUserOp> {
  let joinCallData;
  let nativeTokenValue: bigint = 0n;
  const preUserOp: BatchUserOp = [];
  if (communityId) {
    let joinModuleParam = "0x";
    let joinModule = await client.wallet.ospClient.getJoinModule(communityId);
    const provider = client.wallet.etherProvider;
    const account = client.wallet.account;
    switch (joinModule) {
      case client.wallet.contractConfig.erc20_fee_join_module: {
        const data = await client.wallet.erc20FeeJoinModule.getCommunityData(
          communityId
        );
        const coin = IERC20__factory.connect(data.currency, provider);
        if ((await coin.balanceOf(account.address)) < data.amount) {
          throw new Error(WALLET_ERROR["WALLET_BALANCE_INSUFFICIENT"]);
        } else if (
          (await coin.allowance(account.address, joinModule)) < data.amount
        ) {
          preUserOp.push({
            to: data.currency,
            data: coin.interface.encodeFunctionData("approve", [
              joinModule,
              data.amount,
            ]),
          });
        }
        joinModuleParam = ethers.utils.defaultAbiCoder.encode(
          ["address", "uint256"],
          [data.currency, data.amount]
        );
        break;
      }
      case client.wallet.contractConfig.native_fee_join_module: {
        const data = await client.wallet.nativeFeeJoinModule.getCommunityData(
          communityId
        );
        if ((await account.getBalance()) < data.amount) {
          throw new Error(WALLET_ERROR["WALLET_BALANCE_INSUFFICIENT"]);
        }
        nativeTokenValue = data.amount.toBigInt();
        break;
      }
      case client.wallet.contractConfig.hold_token_join_module: {
        const data = await client.wallet.holdTokenJoinModule.getCommunityData(
          communityId
        );
        let balance;
        if (data.token == "0x0000000000000000000000000000000000000000") {
          //native token
          balance = await account.getBalance();
        } else {
          const token = IERC20__factory.connect(data.token, provider);
          balance = await token.balanceOf(account.address);
        }
        if (balance < data.amount) {
          throw new Error(WALLET_ERROR["WALLET_BALANCE_INSUFFICIENT"]);
        }
        break;
      }
    }
    joinCallData = client.wallet.ospClient.interface.encodeFunctionData(
      "join",
      [communityId, joinModuleParam]
    ) as Hex;
  }
  return [
    ...preUserOp,
    {
      data: joinCallData,
      value: nativeTokenValue,
    },
  ];
}
