//@ts-nocheck
/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface Response {
  /** sub code */
  code?: number;
  /** information for the response */
  msg?: string;
  data?: object;
  errors?: Error[];
}

export interface Error {
  /** error type, eg. Generic, InvalidRequest, ClientForbidden */
  reason?: string;
  /** human-friendly message */
  detail?: string;
  /** chart */
  location?: string;
  /** parameter */
  locationType?: string;
}

/** device type enum */
export enum DeviceTypeEnum {
  UNKNOWN = "UNKNOWN",
  ANDROID = "ANDROID",
  IOS = "IOS",
  WEB = "WEB",
  MANAGER = "MANAGER",
}

export type TransactionResponse = Response & {
  data?: {
    /** txHash if success */
    tx_hash?: string;
    /** txId if indexed by relay */
    tx_id?: string;
    info?: object;
  };
};

export interface Media {
  /** id for resource */
  id?: string;
  /** media url */
  url?: string;
  /** mimeType of media format, image/gif ,video/mp4, audio/mp3 */
  mime_type?: string;
  /** cover url */
  cover?: string;
}

export interface NFT {
  /** nft name */
  name?: string;
  /** nft link for opensocial */
  external_url?: string;
  /** nft image url, ipfs link */
  image?: string;
  /** nft animation url, ipfs link */
  animation_url?: string;
  /** nft desc */
  description?: string;
  attributes?: Record<string, object>;
}

/** device type enum */
export enum BaseMetaTypeEnum {
  Profile = "profile",
  Community = "community",
}

/** post type enum */
export enum PostTypeEnum {
  DRAFT = "DRAFT",
  INDEX = "INDEX",
}

export interface Challenge {
  /** text to sign */
  text?: string;
}

export type ChallengeResponse = Response & {
  data?: Challenge;
};

export interface Token {
  /** 30 minutes access token */
  access_token?: string;
  /** 7 days refresh token */
  refresh_token?: string;
}

export type TokenResponse = Response & {
  data?: Token;
};

export interface AuthRequest {
  /** sign message by challenge text */
  signature: string;
  /** address */
  address: string;
}

/** ThirdAuthSource */
export enum ThirdAuthSourceEnum {
  EXTERNAL_WALLET = "EXTERNAL_WALLET",
  EXTERNAL_SOCIAL = "EXTERNAL_SOCIAL",
}

export interface Activity {
  /** profile info */
  profile?: Profile;
  /** @format uri */
  content_uri?: string;
  reference_module?: ReferenceModule;
  collect_module?: CollectModule;
  title?: string;
  text?: string;
  summary?: string;
  profile_id?: string;
  collect_nft_address?: string;
  view_id?: string;
  /** chain content id */
  content_id?: string;
  /** @format int64 */
  created?: number;
  /** @format int64 */
  share_count?: number;
  /** @format int64 */
  like_count?: number;
  /** @format int64 */
  comment_count?: number;
  /** @format int64 */
  reply_count?: number;
  /** @format int64 */
  favorite_count?: number;
  /** @format int64 */
  view_count?: number;
  /** @format int64 */
  upvote_count?: number;
  /** @format int64 */
  downvote_count?: number;
}

export type ActivityResponse = Response & {
  data?: Activity;
};

export type ActivityPaginationResponse = Response & {
  data?: {
    /**
     * @format int32
     * @min 1
     * @max 20
     */
    limit?: number;
    /** @format int32 */
    total?: number;
    next_token?: string;
    rows?: Activity[];
  };
};

export interface AddActivityRequest {
  /** The hexadecimal community id */
  community_id?: string;
  /**
   * The metadata uploaded somewhere passing in the url to reach it, by
   * @format uri
   */
  content_uri?: string;
  /** content_uri include info JSON to string */
  content?: string;
  collect_module?: CollectModule;
  reference_module?: ReferenceModule;
}

/** activity category */
export enum ActivityCategoryEnum {
  POST = "POST",
  QUESTION = "QUESTION",
  ANSWER = "ANSWER",
}

/** feed ranking enum */
export enum ActivityRankingEnum {
  ACTIVITY_TIME = "ACTIVITY_TIME",
  TOP_COLLECTED = "TOP_COLLECTED",
  TOP_COMMENTED = "TOP_COMMENTED",
  TOP_REPOSTED = "TOP_REPOSTED",
}

export interface GetActivitiesRequest {
  /**
   * The amount of activities requested from the APIs
   * @format int32
   */
  limit?: number;
  /** nextToken */
  next_token?: string;
  /** filter activity category */
  categories?: ActivityCategoryEnum[];
  /** exclude profile ids */
  exclude_profile_ids?: string[];
  /** turn on or off randomizer, not supported */
  randomize?: boolean;
  /** feed ranking enum */
  ranking?: ActivityRankingEnum;
  /** The tenant Ids */
  tenants?: string[];
  /**
   * The tenant Ids
   * @format int64
   */
  timestamp?: number;
}

/** activity category */
export enum CategoryEnum {
  POST = "POST",
  ANSWER = "ANSWER",
  QUESTION = "QUESTION",
}

export interface EnrichOption {
  /** 是否开启enrich */
  enrich?: boolean;
  /** 是否携带最近的互动信息 */
  withRecentReactions?: boolean;
  /**
   * 每次携带的互动信息数量
   * @min 0
   * @max 20
   */
  recentReactionsLimit?: number;
  /** 是否携带操作人的互动关系 */
  withOwnReactions?: boolean;
  /** 携带child信息 */
  withOwnChildren?: boolean;
  /** 是否携带互动数 */
  withReactionCounts?: boolean;
  /** 需要携带的互动类型 */
  reactionKindsFilter?: ReactionKindEnum[];
  /**
   * 是否返回创建者信息
   * @example false
   */
  with_profile?: boolean;
  /**
   * 是否返回板块信息
   * @example false
   */
  with_community?: boolean;
  /** 需要查询的与当前登录用户的relation */
  with_relations?: RelationEnum[];
}

export interface Feed {
  /** display content id */
  view_id?: string;
  /** chain content id */
  content_id?: string;
  /** profile id */
  profile_id?: string;
  /** activity status */
  status?: FeedStatusEnum;
  /** activity category */
  category?: CategoryEnum;
  /** 标题 */
  title?: string;
  /** 摘要内容 */
  summary?: string;
  /** 全文内容 */
  text?: string;
  medias?: Media[];
  /** 链接内容 */
  link?: string;
  /** 链接展示文案 */
  link_text?: string;
  /** 链接来源 */
  link_author?: string;
  /** 主题内容 */
  community_id?: string;
  /** 包含的标签或关键字 */
  keywords?: string[];
  /**
   * 创建时间
   * @format int64
   */
  created?: number;
  /**
   * 修改时间
   * @format int64
   */
  modified?: number;
  /**
   * 分享数
   * @format int64
   */
  share_count?: number;
  /**
   * 点赞数
   * @format int64
   */
  like_count?: number;
  /**
   * 评论数
   * @format int64
   */
  comment_count?: number;
  /**
   * 回复数
   * @format int64
   */
  reply_count?: number;
  /**
   * 收藏数
   * @format int64
   */
  favorite_count?: number;
  /**
   * 浏览数
   * @format int64
   */
  view_count?: number;
  /**
   * 同意数
   * @format int64
   */
  upvote_count?: number;
  /**
   * 反对数
   * @format int64
   */
  downvote_count?: number;
  /**
   * 提问的回答数
   * @format int64
   */
  child_activity_count?: number;
  /**
   * 提问的回答点赞数
   * @format int64
   */
  child_activity_like_count?: number;
  /**
   * 提问的最新一次回答时间
   * @format int64
   */
  latest_activity_time?: number;
  /** 是否屏蔽了该创建者 */
  blocking?: boolean;
  /** 是否关注了该创建者 */
  following?: boolean;
  /** profile info */
  profile?: Profile;
  /** 最近互动列表 */
  recent?: Record<string, Reaction[]>;
  /** 是否产生过互动 */
  owns?: Record<string, number>;
  collection?: Feed;
  activity?: Feed;
  /** community info */
  community?: Community;
  /** 登录用户与返回用户的关系 */
  relations?: RelationEnum[];
}

export type FeedPaginationResponse = Response & {
  data?: {
    /**
     * request data count
     * @format int32
     * @min 1
     * @max 20
     */
    limit?: number;
    /**
     * 返回的总页数，默认不查询总页数，返回0
     * @format int32
     */
    total?: number;
    /** 翻页token */
    nextToken?: string;
    rows?: Feed[];
  };
};

/** feed ranking enum */
export enum FeedRankingEnum {
  ACTIVITY_TIME = "ACTIVITY_TIME",
  RECOMMEND_TIME = "RECOMMEND_TIME",
  RECOMMEND_LEVEL = "RECOMMEND_LEVEL",
  HOT_VALUE = "HOT_VALUE",
}

export type FeedResponse = Response & {
  data?: Feed;
};

/** activity status */
export enum FeedStatusEnum {
  NORMAL = "NORMAL",
  INNER = "INNER",
  STOP = "STOP",
}

export interface ListFeedRequest {
  /**
   * The amount of activities requested from the APIs
   * @format int32
   */
  limit?: number;
  /** rank order */
  reverse?: boolean;
  /** nextToken */
  next_token?: string;
  /**
   * 过滤掉的viewId
   * @maxItems 5
   */
  exclude_view_ids?: string[];
  /** feed ranking enum */
  ranking?: FeedRankingEnum;
  enrichOption?: EnrichOption;
  /** 标记已读 */
  mark_read?: boolean;
  /** feed params for flat */
  params?: Record<string, any>;
}

/** profile info */
export interface Profile {
  /** profile id */
  profile_id?: string;
  handle?: string;
  bio?: string;
  background?: string;
  /** picture(url,height/width/mimeType...) or NftImage(contractAddress,tokenId,uri) */
  avatar?: string;
  /** todo support? */
  metadata?: object;
  /** owner address */
  owner?: string;
  attributes?: ProfileAttribute[];
  dispatcher?: Dispatcher;
  /** The follow Module */
  follow_module?: object;
  /** Follow nft address */
  follow_nft_address?: string;
  /** isFollowing */
  following?: boolean;
  /** following token id */
  following_token_id?: string;
  /**
   * 1 off chain ,2on chain
   * @format int64
   */
  chain_following?: number;
  /** isFollowed */
  followed?: boolean;
  /** @format int64 */
  following_count?: number;
  /** @format int64 */
  chain_following_count?: number;
  /** @format int64 */
  follower_count?: number;
  /** @format int64 */
  chain_follower_count?: number;
  /** @format int64 */
  subscribe_count?: number;
  /** @format int64 */
  activity_count?: number;
  /** @format int64 */
  join_count?: number;
  /** @format int64 */
  activity_like_count?: number;
}

export interface ProfileAttribute {
  /** The display type */
  display_type?: string;
  /** identifier of this attribute, we will update by this id */
  key?: string;
  /** The trait type - can be anything its the name it will render so include spaces */
  trait_type?: string;
  /** Value attribute */
  value?: string;
}

export type ProfileResponse = Response & {
  /** profile info */
  data?: Profile;
};

export type ProfilePaginationResponse = Response & {
  data?: {
    /**
     * @format int32
     * @min 1
     * @max 20
     */
    limit?: number;
    /** @format int32 */
    total?: number;
    next_token?: string;
    rows?: Profile[];
  };
};

export interface Dispatcher {
  /** wallet address of dispatcher */
  address?: string;
  /** use the built-in dispatcher on the API */
  canRelay?: boolean;
}

export interface ListProfileRequest {
  /** user owner wallet address */
  address?: string;
  /** user handle */
  handles?: string[];
  /** community id */
  community_id?: string;
  /** profile rank kind */
  ranking?: ProfileRankingEnum;
  /** user id */
  ids?: string[];
  /** nextToken for pagination */
  next_token?: string;
  /**
   * limit for pagination
   * @format int32
   * @min 1
   * @max 20
   */
  limit?: number;
}

/** profile rank kind */
export enum ProfileRankingEnum {
  RECOMMEND = "RECOMMEND",
}

/** Asset Info */
export interface Asset {
  /** contract address */
  contract_address?: string;
  /** token id */
  token_id?: string;
  /** chain id */
  chain_id?: number;
  nft?: NFT;
}

/** nft asset type */
export enum AssetTypeEnum {
  NFT = "NFT",
  SBT = "SBT",
}

export type AssetPaginationResponse = Response & {
  data?: {
    /**
     * @format int32
     * @min 1
     * @max 20
     */
    limit?: number;
    /** @format int32 */
    total?: number;
    next_token?: string;
    rows?: Asset[];
  };
};

/** 查询动态列表 */
export interface ListStoryRequest {
  /**
   * @min 1
   * @max 20
   */
  limit?: number;
  next_token?: string;
  profile_id?: string;
  /** @maxItems 5 */
  zones?: string[];
}

/** 动态对象 */
export interface Story {
  /** 主键id */
  id?: string;
  /**
   * 创建时间
   * @format int64
   */
  created?: number;
  /** 点赞或者关注用户时链上id，为空代表链下操作 */
  content_id?: string;
  /** question status enum */
  verb?: StoryVerbEnum;
  activity?: Feed;
  reaction?: Feed;
  /** 回复 */
  child_reaction?: Feed;
  /** community info */
  community?: Community;
  /** profile info */
  profile?: Profile;
}

export type StoryPaginationResponse = Response & {
  data?: {
    /**
     * 每次请求数
     * @format int32
     * @min 1
     * @max 20
     */
    limit?: number;
    /**
     * 返回的总页数，默认不查询总页数，返回0
     * @format int32
     */
    total?: number;
    /** 翻页token */
    next_token?: string;
    rows?: Story[];
  };
};

/** question status enum */
export enum StoryVerbEnum {
  LIKE_POST = "LIKE_POST",
  COMMENT = "COMMENT",
  REPLY = "REPLY",
  FOLLOW = "FOLLOW",
  JOIN_COMMUNITY = "JOIN_COMMUNITY",
  CREATE_COMMUNITY = "CREATE_COMMUNITY",
}

/** community info */
export interface Community {
  /** 社区id */
  id?: string;
  /** 社区唯一名 */
  handle?: string;
  /** display */
  display_name?: string;
  /** 描述 */
  desc?: string;
  /** banner url */
  banner?: string;
  /** template id */
  template?: string;
  /** 创建用户ID */
  profile_id?: string;
  /** logo */
  logo?: string;
  /**
   * 帖子数
   * @format int64
   */
  activity_count?: number;
  /**
   * 社区关注数
   * @format int64
   */
  member_count?: number;
  /** profile info */
  profile?: Profile;
  /** isJoining */
  joining?: boolean;
  /** @format int64 */
  modified?: number;
  /** @format int64 */
  draft_time?: number;
  /** join token id */
  join_token_id?: string;
  /** join_nft */
  join_nft?: string;
  nft?: NFT;
  tags?: CommunityTag[];
  /** bind domain name */
  domain?: string;
  /** status ----0 正常，1 正在上链 */
  status?: string;
  /** template_body */
  template_body?: string;
  /** join_module */
  join_module?: object;
  /** join_module_details */
  join_module_details?: object;
  /** is query from draft */
  is_draft?: boolean;
  /** community info */
  draft?: Community;
  /** language data */
  language?: string;
  /** token_id data */
  token_id?: string;
  rules?: string[];
}

export type CommunityPaginationResponse = Response & {
  data?: {
    /**
     * 每次请求数
     * @format int32
     * @min 1
     * @max 20
     */
    limit?: number;
    /**
     * 返回的总页数，默认不查询总页数，返回0
     * @format int32
     */
    total?: number;
    /** 翻页token */
    next_token?: string;
    rows?: Community[];
  };
};

/** community ranking type */
export enum CommunityRankingEnum {
  ORDER = "ORDER",
  MODIFIED = "MODIFIED",
  CREATED = "CREATED",
  RECOMMEND = "RECOMMEND",
  DRAFT_TIME = "DRAFT_TIME",
}

export type CommunityResponse = Response & {
  /** community info */
  data?: Community;
};

/** query communities */
export interface ListCommunityRequest {
  /** community ranking type */
  ranking: CommunityRankingEnum;
  /** query by custom domain/hostname */
  domain?: string;
  /** query by handle */
  handle?: string;
  /**
   * @min 1
   * @max 20
   */
  limit: number;
  next_token?: string;
  /**
   * community creator
   * @default false
   */
  with_profile?: boolean;
  profile_id?: string;
  tag_id?: string;
  language?: string;
}

export interface UpdateCommunityRequest {
  /** post type enum */
  type?: PostTypeEnum;
  desc_medias?: Media[];
  fields?: Record<string, object>;
}

/** community creation condition */
export enum ConditionEnum {
  SLOT_NFT_CONDITION = "SLOT_NFT_CONDITION",
  WHITELIST_ADDRESS_CONDITION = "WHITELIST_ADDRESS_CONDITION",
}

export interface ConditionParam {
  /** community creation condition */
  type?: ConditionEnum;
  param?: {
    slot_nft_condition?: SlotNFTConditionParamData;
  };
}

export interface SlotNFTConditionParamData {
  /** slot nft address */
  slot_nft_address?: string;
  /**
   * token id
   * @format int64
   */
  token_id?: number;
}

export interface CommunityRequest {
  handle?: string;
  /** show name */
  display_name?: string;
  /** community desc */
  desc?: string;
  /** logo url */
  logo?: string;
  /** banner url */
  banner?: string;
  /** language */
  language?: string;
  /** rules */
  rules?: string[];
  /** template id */
  template?: string;
  /** template 内容 */
  template_body?: string;
  desc_medias?: Media[];
  /**
   * tag id
   * @maxItems 3
   */
  tags?: string[];
  condition?: ConditionParam;
  join_module?: JoinModule;
  token_id?: string;
  /** post type enum */
  type?: PostTypeEnum;
}

export type ConditionResponse = Response & {
  data?: {
    /** 是否拥有社区创建权限，有过有返回true，没有返回false */
    access?: boolean;
    /** 如果在创建社区白名单，可以直接创建社区，页面上不需要任何交互 */
    whitelist?: boolean;
    /** 返回用户持有的slot nft，最多返回10条，通过slot nft创建社区，需要用户进行选择 */
    availableNFTs?: SlotNFT;
  };
};

export interface SlotNFT {
  /** contract address */
  contract_address?: string;
  /** user's token id of nft */
  token_id?: string;
  nft?: NFT;
}

export interface CommunityTag {
  /** tag id */
  id?: string;
  /** tag name */
  name?: string;
}

export type CommunityTagResponse = Response & {
  data?: {
    /**
     * 每次请求数
     * @format int32
     * @min 1
     * @max 20
     */
    limit?: number;
    /**
     * 返回的总页数，默认不查询总页数，返回0
     * @format int32
     */
    total?: number;
    /** 翻页token */
    next_token?: string;
    rows?: CommunityTag[];
  };
};

export interface RecipientPercent {
  /** recipient contract address */
  recipient?: string;
  /**
   * should be between 1 and 100
   * @format float
   */
  percent?: number;
}

/** collect module enum */
export enum CollectModuleEnum {
  FREE_COLLECT_MODULE = "FREE_COLLECT_MODULE",
}

export interface CollectModule {
  /** collect module enum */
  type: CollectModuleEnum;
  /** You only have to enter one   */
  init?: {
    free_collect_module?: FreeCollectModule;
  };
}

export interface FreeCollectModule {
  /** only for my follower */
  only_follower: boolean;
}

export interface CollectModuleParam {
  /** collect module enum */
  type: CollectModuleEnum;
  /** 根据具体的Module类型，填写对应的数据 */
  data?: {
    free_collect_module?: object;
  };
}

export enum ReferenceModuleEnum {
  FOLLOWER_ONLY_REFERENCE_MODULE = "FOLLOWER_ONLY_REFERENCE_MODULE",
  NULL_REFERENCE_MODULE = "NULL_REFERENCE_MODULE",
}

export interface ReferenceModule {
  type: ReferenceModuleEnum;
  /** 根据具体的Module类型，填写对应的数据 */
  init?: {
    null_reference_module?: object;
    follower_only_reference_module?: object;
  };
}

export interface ReferenceModuleParam {
  type: ReferenceModuleEnum;
  /** 根据具体的Module类型，填写对应的数据 */
  data?: {
    null_reference_module?: object;
    follower_only_reference_module?: object;
  };
}

export interface DegreesOfSeparationReferenceModule {
  comments_restricted?: boolean;
  mirror_restricted?: boolean;
  /** Degrees of separation */
  degrees_of_separation?: number;
}

/** follow module enum */
export enum FollowModuleEnum {
  NULL_FOLLOW_MODULE = "NULL_FOLLOW_MODULE",
}

export interface FollowModule {
  /** follow module enum */
  type: FollowModuleEnum;
  /** follow module */
  init?: {
    null_follow_module?: object;
  };
}

/** follow module redeem parameter */
export interface FollowModuleParam {
  /** follow module enum */
  type: FollowModuleEnum;
  /** You only have to enter one  */
  data?: {
    null_follow_module?: object;
  };
}

export interface JoinModule {
  /** join module enum */
  type: JoinModuleEnum;
  /** join module */
  init?: {
    null_join_module?: object;
    erc20_fee_join_module?: ERC20FeeJoinModule;
    native_fee_join_module?: NativeFeeJoinModule;
    hold_token_join_module?: HoldTokenJoinModule;
  };
}

export interface ERC20FeeJoinModule {
  /** currency contract address */
  currency_address?: string;
  /** It could have the entire precision of the Asset or be truncated to the last significant decimal */
  amount_uint?: string;
  /** The follow module recipient address */
  recipient_address?: string;
}

export interface NativeFeeJoinModule {
  /** It could have the entire precision of the Asset or be truncated to the last significant decimal */
  amount_uint?: string;
  /** The join module recipient address */
  recipient_address?: string;
}

export interface HoldTokenJoinModule {
  /** token contract address */
  token_address?: string;
  amount_uint?: string;
}

/** join module enum */
export enum JoinModuleEnum {
  NULL_JOIN_MODULE = "NULL_JOIN_MODULE",
  ERC20FEEJOINMODULE = "ERC20_FEE_JOIN_MODULE",
  HOLD_TOKEN_JOIN_MODULE = "HOLD_TOKEN_JOIN_MODULE",
  NATIVE_FEE_JOIN_MODULE = "NATIVE_FEE_JOIN_MODULE",
}

/** join module parameter */
export interface JoinModuleParam {
  /** join module enum */
  type: JoinModuleEnum;
  /** You only have to enter one  */
  data?: {
    null_join_module?: object;
    erc20_fee_join_module?: FeeModuleParamData;
    native_fee_join_module?: object;
    hold_token_join_module?: object;
  };
}

export interface FeeModuleParamData {
  /** currency contract address */
  currency_address?: string;
  /** It could have the entire precision of the Asset or be truncated to the last significant decimal */
  amount_uint?: string;
}

export interface Broadcast {
  tx_hash: string;
}

export type BroadcastResponse = Response & {
  data?: Broadcast;
};

export interface BroadcastRequest {
  id: string;
  signature: string;
}

export interface StoreRequest {
  tx_hash: string;
  topic?: string;
}

export interface TypeData {
  domain: object;
  types: object;
  message?: object;
  primaryType?: string;
}

export type TypeDataResponse = Response & {
  data?: {
    type_data: TypeData;
    broadcast_id: string;
  };
};

export interface ContentReactionRequest {
  referenced_profile_id?: string;
  /**
   * content id
   * @format string
   */
  referenced_content_id?: string;
  /**
   * The metadata uploaded somewhere passing in the url to reach it
   * @format uri
   */
  content_uri?: string;
  /** content reaction kind */
  kind?: ContentTypeEnum;
  reference_module?: ReferenceModule;
  reference_module_param?: ReferenceModuleParam;
}

export interface ReactionRequest {
  referenced_profile_id?: string;
  /**
   * content id
   * @format string
   */
  referenced_content_id?: string;
  /** reaction kind */
  kind?: ReactionTypeEnum;
  reference_module_param?: ReferenceModuleParam;
  reaction_param?: {
    like?: {
      is_like?: boolean;
    };
  };
}

/** content reaction kind */
export enum ContentTypeEnum {
  Post = "Post",
  Comment = "Comment",
  Mirror = "Mirror",
}

/** reaction kind */
export enum ReactionTypeEnum {
  LIKE = "LIKE",
}

export interface ProfileRequest {
  handle?: string;
  follow_module?: FollowModule;
}

export interface SetJoinModuleRequest {
  community_id?: string;
  join_module?: JoinModule;
}

export interface SetFollowModuleRequest {
  profile_id?: string;
  follow_module?: FollowModule;
}

export interface ActivityRequest {
  /** The hexadecimal community id */
  community_id?: string;
  /**
   * The metadata uploaded somewhere passing in the url to reach it
   * @format uri
   */
  content_uri?: string;
  collect_module?: CollectModule;
  reference_module?: ReferenceModule;
}

/** follow user */
export interface FollowRequest {
  /** @format string */
  id?: string;
  /** follow module redeem parameter */
  follow_module_param?: FollowModuleParam;
}

/** join community */
export interface JoinRequest {
  /** @format string */
  id?: string;
  /** join module parameter */
  join_module_param?: JoinModuleParam;
}

export interface AddReactionRequest {
  referenced_profile_id?: string;
  /**
   * display content id
   * @format string
   */
  referenced_view_id?: string;
  /**
   * The metadata uploaded somewhere passing in the url to reach it
   * @format uri
   */
  content_uri?: string;
  /** content_uri JSON to string */
  content?: string;
  /** reaction kind */
  kind?: ReactionKindEnum;
  collect_module?: CollectModule;
  reference_module?: ReferenceModule;
  collect_module_param?: CollectModuleParam;
  reference_module_param?: ReferenceModuleParam;
}

/** reaction kind */
export enum ReactionKindEnum {
  COMMENT = "COMMENT",
  REPLY = "REPLY",
  LIKE = "LIKE",
  SHARE = "SHARE",
  FAVORITE = "FAVORITE",
  UPVOTE = "UPVOTE",
  DONWVOTE = "DONWVOTE",
}

/** reaction request */
export interface ListReactionRequest {
  /** reaction kind */
  kind?: ReactionKindEnum;
  /** reaction ranking type */
  ranking?: ReactionRankingEnum;
  /**
   * @min 1
   * @max 20
   */
  limit?: number;
  next_token?: string;
  /** @example false */
  with_activity?: boolean;
  /** @example false */
  with_profile?: boolean;
  /** @example false */
  with_child_count?: boolean;
  /** @example false */
  with_own_children?: boolean;
  /** @example false */
  with_child?: boolean;
  /**
   * @min 1
   * @max 10
   */
  child_limit?: number;
  /** 需要查询的kind，目前仅支持REPLY */
  child_kinds?: ReactionKindEnum[];
  /** @example false */
  with_target?: boolean;
  /** @example false */
  with_parent?: boolean;
  /**
   * 过滤掉的viewId
   * @maxItems 5
   */
  exclude_view_ids?: string[];
}

export type ReactionPaginationResponse = Response & {
  data?: {
    /**
     * limit
     * @format int32
     * @min 1
     * @max 20
     */
    limit?: number;
    /**
     * total
     * @format int32
     */
    total?: number;
    /** nextToken */
    next_token?: string;
    rows?: Reaction[];
  };
};

/** reaction ranking type */
export enum ReactionRankingEnum {
  TIME = "TIME",
  HOT = "HOT",
  TOP = "TOP",
}

export interface Reaction {
  /** id */
  id?: string;
  /** user id */
  profile_id?: string;
  /** dispay content id */
  view_id?: string;
  /** chain content id */
  content_id?: string;
  /** @format int64 */
  created?: number;
  /**
   * like count
   * @format int64
   */
  like_count?: number;
  /**
   * reply count
   * @format int64
   */
  reply_count?: number;
  /** reaction kind */
  kind?: ReactionKindEnum;
  collect_module?: CollectModule;
  reference_module?: ReferenceModule;
  /** profile info */
  profile?: Profile;
  target?: ParentReaction;
  /** target view id */
  target_view_id?: string;
  data?: AdditionalData;
  activity?: Feed;
  /** 最近互动列表 */
  children?: Record<string, ChildReaction[]>;
  /** my own reaction */
  owns?: Record<string, number>;
}

export type ReactionResponse = Response & {
  data?: Reaction;
};

export interface AdditionalData {
  /** 带有内容的reaction，比如评论/回复 */
  text?: string;
  /** 带有图片的reaction，比如评论/回复 */
  images?: string[];
  /** 场景 */
  scene?: string;
  /** 互动目标ID */
  target?: string;
}

export interface ParentReaction {
  id?: string;
  /** 行为人 */
  profile_id?: string;
  /** reaction kind */
  kind?: ReactionKindEnum;
  /** 目标id */
  target_view_id?: string;
  data?: AdditionalData;
  /** @format int64 */
  created?: number;
  /** @format int64 */
  modified?: number;
  /**
   * 点赞数
   * @format int64
   */
  like_count?: number;
  /**
   * 回复数
   * @format int64
   */
  reply_count?: number;
  /** profile info */
  profile?: Profile;
}

export interface ChildReaction {
  /** id */
  id?: string;
  /** profile id */
  profile_id?: string;
  /** reaction kind */
  kind?: ReactionKindEnum;
  /** 目标id */
  target_view_id?: string;
  data?: AdditionalData;
  /** @format int64 */
  created?: number;
  /** @format int64 */
  modified?: number;
  /**
   * 点赞数
   * @format int64
   */
  like_count?: number;
  /**
   * 回复数
   * @format int64
   */
  reply_count?: number;
  /** chain content id */
  content_id?: string;
  /** profile info */
  profile?: Profile;
  target?: ParentReaction;
  /** 是否产生过互动 */
  owns?: Record<string, number>;
}

/** query relation */
export interface ListRelationRequest {
  /**
   * query reverse relation
   * @default false
   */
  with_reverse?: boolean;
  /**
   * @min 1
   * @max 20
   */
  limit?: number;
  next_token?: string;
  /** @maxItems 20 */
  include_target_ids?: string[];
  /** @maxItems 5 */
  exclude_target_ids?: string[];
}

/** relation enum */
export enum RelationEnum {
  JOIN = "JOIN",
  FOLLOW = "FOLLOW",
  BLOCK = "BLOCK",
}

/** follow user */
export interface AddFollowingRequest {
  /** follow module redeem parameter */
  follow_module_param?: FollowModuleParam;
}

/** follow user */
export interface AddJoiningRequest {
  /** join module parameter */
  join_module_param?: JoinModuleParam;
}

export type ContractAddressResponse = Response & {
  data?: {
    /** osp contract address */
    osp?: string;
    /** community nft contract address */
    community_nft?: string;
    /** slot nft condition address */
    slot_nft_condition?: string;
    /** whitelist address condition address */
    whitelist_address_condition?: string;
    /** slot nft addresses */
    slot_nft?: string[];
    /** free collect module address */
    free_collect_module?: string;
    /** follower only reference module address */
    follower_only_reference_module?: string;
    /** ERC20 fee join module address */
    erc20_fee_join_module?: string;
    /** native token fee join module address */
    native_fee_join_module?: string;
    hold_token_join_module?: string;
  };
};

export type ContractConfigResponse = Response & {
  data?: {
    /** osp contract address */
    osp?: string;
    /** community nft contract address */
    community_nft?: string;
    /** slot nft condition address */
    slot_nft_condition?: string;
    /** whitelist address condition address */
    whitelist_address_condition?: string;
    /** slot nft addresses */
    slot_nft?: string[];
    /** free collect module address */
    free_collect_module?: string;
    /** follower only reference module address */
    follower_only_reference_module?: string;
    /** ERC20 fee join module address */
    erc20_fee_join_module?: string;
    /** native token fee join module address */
    native_fee_join_module?: string;
    hold_token_join_module?: string;
    coin_mint_plugin_address?: string;
    entry_point_address?: string;
    account_factory_address?: string;
    paymaster_address?: string;
    chain_id?: string;
    alchemy_api?: string;
  };
};

export interface UserOperation {
  sender?: string;
  nonce?: string;
  init_code?: string;
  call_data?: string;
  call_gas_limit?: string;
  verification_gas_limit?: string;
  pre_verification_gas?: string;
  max_fee_per_gas?: string;
  max_priority_fee_per_gas?: string;
}

export interface PaymasterAndDataRequest {
  user_operation?: UserOperation;
  /** subsidy or erc20 */
  type?: "SUBSIDY" | "ERC20";
  token_address?: string;
}

export type PaymasterAndDataResponse = Response & {
  data?: {
    paymaster_and_data?: string;
  };
};

export type PaymasterCheckResponse = Response & {
  data?: {
    /** subsidize or not */
    subsidy?: boolean;
  };
};

/** token info */
export interface CoinToken {
  /** id（后端生成） */
  id?: string;
  plugin_id?: string;
  /** community list */
  communities?: Community[];
  /** token name */
  name?: string;
  /** token symbol */
  symbol?: string;
  /** token banner */
  banner?: string;
  /** token address （合约地址） */
  contract_address?: string;
  /**
   * 领取开始时间
   * @format int64
   */
  claim_start_time?: number;
  /**
   * 完成时间
   * @format int64
   */
  complete_time?: number;
  /** 支付币种信息 */
  pay_currency?: string;
  /**
   * 领取结束时间
   * @format int64
   */
  claim_end_time?: number;
  /** 代币总领取供应数量（初始发行量） */
  claim_supply?: string;
  /** 单个钱包最大领取量 */
  limit_per_claim?: string;
  /**
   * precision （代币精度）
   * @format int64
   */
  precision?: number;
  /** user address （创建者地址） */
  owner_address?: string;
  /** token分配比例（json存储只读） */
  distribution?: string;
  /** 领取支付价格（阶梯收费标准后端直接json存储只读做展示用） */
  claim_price?: string;
  /** 已领取数量(用来计算进度) */
  claimed_amount?: string;
  /**
   * 已领取人数
   * @format int64
   */
  claimed_user_count?: number;
  /** 最近领取人 */
  claimed_user?: Profile[];
  remark?: string;
  /** @format int64 */
  modified?: number;
  /** @format int64 */
  created?: number;
}

export interface GetTokenRequest {
  /** @format int32 */
  limit?: number;
  /** nextToken */
  next_token?: string;
  /**
   * create start time condition
   * @format int64
   */
  create_start_time?: number;
  /**
   * create end time condition
   * @format int64
   */
  create_end_time?: number;
  /** token symbol */
  symbol?: string;
  /** community id */
  community_id?: string;
  /**
   * Most recently claimed user
   * @default false
   */
  with_profile?: boolean;
  /**
   * community
   * @default false
   */
  with_community?: boolean;
}

export type TokenPaginationResponse = Response & {
  data?: {
    /**
     * @format int32
     * @min 1
     * @max 20
     */
    limit?: number;
    /** @format int32 */
    total?: number;
    next_token?: string;
    rows?: CoinToken[];
  };
};

export type CoinTokenResponse = Response & {
  /** token info */
  data?: CoinToken;
};

export interface UpdateTokenRequest {
  fields?: Record<string, object>;
}

export type QueryParamsType = Record<string | number, any>;
export type ResponseFormat = keyof Omit<Body, "body" | "bodyUsed">;

export interface FullRequestParams extends Omit<RequestInit, "body"> {
  /** set parameter to `true` for call `securityWorker` for this request */
  secure?: boolean;
  /** request path */
  path: string;
  /** content type of request body */
  type?: ContentType;
  /** query params */
  query?: QueryParamsType;
  /** format of response (i.e. response.json() -> format: "json") */
  format?: ResponseFormat;
  /** request body */
  body?: unknown;
  /** base url */
  baseUrl?: string;
  /** request cancellation token */
  cancelToken?: CancelToken;
}

export type RequestParams = Omit<
  FullRequestParams,
  "body" | "method" | "query" | "path"
>;

export interface ApiConfig<SecurityDataType = unknown> {
  baseUrl?: string;
  baseApiParams?: Omit<RequestParams, "baseUrl" | "cancelToken" | "signal">;
  securityWorker?: (
    securityData: SecurityDataType | null
  ) => Promise<RequestParams | void> | RequestParams | void;
  customFetch?: typeof fetch;
}

export interface HttpResponse<D extends unknown, E extends unknown = unknown>
  extends Response {
  data: D;
  error: E;
}

type CancelToken = Symbol | string | number;

export enum ContentType {
  Json = "application/json",
  FormData = "multipart/form-data",
  UrlEncoded = "application/x-www-form-urlencoded",
  Text = "text/plain",
}

export class HttpClient<SecurityDataType = unknown> {
  public baseUrl: string =
    "https://{region}-{environment}.opensocial.{domain}/v2/";
  private securityData: SecurityDataType | null = null;
  private securityWorker?: ApiConfig<SecurityDataType>["securityWorker"];
  private abortControllers = new Map<CancelToken, AbortController>();
  private customFetch = (...fetchParams: Parameters<typeof fetch>) =>
    fetch(...fetchParams);

  private baseApiParams: RequestParams = {
    credentials: "same-origin",
    headers: {},
    redirect: "follow",
    referrerPolicy: "no-referrer",
  };

  constructor(apiConfig: ApiConfig<SecurityDataType> = {}) {
    Object.assign(this, apiConfig);
  }

  public setSecurityData = (data: SecurityDataType | null) => {
    this.securityData = data;
  };

  protected encodeQueryParam(key: string, value: any) {
    const encodedKey = encodeURIComponent(key);
    return `${encodedKey}=${encodeURIComponent(
      typeof value === "number" ? value : `${value}`
    )}`;
  }

  protected addQueryParam(query: QueryParamsType, key: string) {
    return this.encodeQueryParam(key, query[key]);
  }

  protected addArrayQueryParam(query: QueryParamsType, key: string) {
    const value = query[key];
    return value.map((v: any) => this.encodeQueryParam(key, v)).join("&");
  }

  protected toQueryString(rawQuery?: QueryParamsType): string {
    const query = rawQuery || {};
    const keys = Object.keys(query).filter(
      (key) => "undefined" !== typeof query[key]
    );
    return keys
      .map((key) =>
        Array.isArray(query[key])
          ? this.addArrayQueryParam(query, key)
          : this.addQueryParam(query, key)
      )
      .join("&");
  }

  protected addQueryParams(rawQuery?: QueryParamsType): string {
    const queryString = this.toQueryString(rawQuery);
    return queryString ? `?${queryString}` : "";
  }

  private contentFormatters: Record<ContentType, (input: any) => any> = {
    [ContentType.Json]: (input: any) =>
      input !== null && (typeof input === "object" || typeof input === "string")
        ? JSON.stringify(input)
        : input,
    [ContentType.Text]: (input: any) =>
      input !== null && typeof input !== "string"
        ? JSON.stringify(input)
        : input,
    [ContentType.FormData]: (input: any) =>
      Object.keys(input || {}).reduce((formData, key) => {
        const property = input[key];
        formData.append(
          key,
          property instanceof Blob
            ? property
            : typeof property === "object" && property !== null
            ? JSON.stringify(property)
            : `${property}`
        );
        return formData;
      }, new FormData()),
    [ContentType.UrlEncoded]: (input: any) => this.toQueryString(input),
  };

  protected mergeRequestParams(
    params1: RequestParams,
    params2?: RequestParams
  ): RequestParams {
    return {
      ...this.baseApiParams,
      ...params1,
      ...(params2 || {}),
      headers: {
        ...(this.baseApiParams.headers || {}),
        ...(params1.headers || {}),
        ...((params2 && params2.headers) || {}),
      },
    };
  }

  protected createAbortSignal = (
    cancelToken: CancelToken
  ): AbortSignal | undefined => {
    if (this.abortControllers.has(cancelToken)) {
      const abortController = this.abortControllers.get(cancelToken);
      if (abortController) {
        return abortController.signal;
      }
      return void 0;
    }

    const abortController = new AbortController();
    this.abortControllers.set(cancelToken, abortController);
    return abortController.signal;
  };

  public abortRequest = (cancelToken: CancelToken) => {
    const abortController = this.abortControllers.get(cancelToken);

    if (abortController) {
      abortController.abort();
      this.abortControllers.delete(cancelToken);
    }
  };

  public request = async <T = any, E = any>({
    body,
    secure,
    path,
    type,
    query,
    format,
    baseUrl,
    cancelToken,
    ...params
  }: FullRequestParams): Promise<HttpResponse<T, E>> => {
    const secureParams =
      ((typeof secure === "boolean" ? secure : this.baseApiParams.secure) &&
        this.securityWorker &&
        (await this.securityWorker(this.securityData))) ||
      {};
    const requestParams = this.mergeRequestParams(params, secureParams);
    const queryString = query && this.toQueryString(query);
    const payloadFormatter = this.contentFormatters[type || ContentType.Json];
    const responseFormat = format || requestParams.format;

    return this.customFetch(
      `${baseUrl || this.baseUrl || ""}${path}${
        queryString ? `?${queryString}` : ""
      }`,
      {
        ...requestParams,
        headers: {
          ...(requestParams.headers || {}),
          ...(type && type !== ContentType.FormData
            ? { "Content-Type": type }
            : {}),
        },
        signal: cancelToken
          ? this.createAbortSignal(cancelToken)
          : requestParams.signal,
        body:
          typeof body === "undefined" || body === null
            ? null
            : payloadFormatter(body),
      }
    ).then(async (response) => {
      const r = response as HttpResponse<T, E>;
      r.data = null as unknown as T;
      r.error = null as unknown as E;

      const data = !responseFormat
        ? r
        : await response[responseFormat]()
            .then((data) => {
              if (r.ok) {
                r.data = data;
              } else {
                r.error = data;
              }
              return r;
            })
            .catch((e) => {
              r.error = e;
              return r;
            });

      if (cancelToken) {
        this.abortControllers.delete(cancelToken);
      }

      if (!response.ok) throw data;
      return data;
    });
  };
}

/**
 * @title Open Social API
 * @version 2.0.0
 * @baseUrl https://{region}-{environment}.opensocial.{domain}/v2/
 *
 * The API for Open Social Project
 */
export class Api<
  SecurityDataType extends unknown
> extends HttpClient<SecurityDataType> {
  auth = {
    /**
     * @description get challenge by address
     *
     * @tags Authentication
     * @name GetChallenge
     * @summary /auth/challenge
     * @request GET:/auth/challenge
     * @secure
     */
    getChallenge: (
      query: {
        /**
         * wallet address
         * @example "0xD8805CEcaD06E93Fc0976Ac8d094beeEA269ae37"
         */
        address: string;
      },
      params: RequestParams = {}
    ) =>
      this.request<ChallengeResponse, void>({
        path: `/auth/challenge`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description login by challenge
     *
     * @tags Authentication
     * @name Authenticate
     * @summary /auth/login
     * @request POST:/auth/login
     * @secure
     */
    authenticate: (data: AuthRequest, params: RequestParams = {}) =>
      this.request<TokenResponse, void>({
        path: `/auth/login`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description verify access token
     *
     * @tags Authentication
     * @name Verify
     * @summary /auth/verify
     * @request POST:/auth/verify
     * @secure
     */
    verify: (
      query: {
        /**
         * verify access token
         * @example ""
         */
        access_token: string;
      },
      params: RequestParams = {}
    ) =>
      this.request<Response, void>({
        path: `/auth/verify`,
        method: "POST",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description refresh access token
     *
     * @tags Authentication
     * @name RefreshToken
     * @summary /auth/refresh
     * @request POST:/auth/refresh
     * @secure
     */
    refreshToken: (
      query: {
        /**
         * refresh access token
         * @example ""
         */
        refresh_token: string;
      },
      params: RequestParams = {}
    ) =>
      this.request<TokenResponse, void>({
        path: `/auth/refresh`,
        method: "POST",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),
  };
  activities = {
    /**
     * No description
     *
     * @tags Activity
     * @name GetActivities
     * @summary /activities
     * @request GET:/activities
     * @secure
     */
    getActivities: (
      query: {
        getActivitiesRequest: GetActivitiesRequest;
      },
      params: RequestParams = {}
    ) =>
      this.request<ActivityPaginationResponse, any>({
        path: `/activities`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description ### 该接口同时支持链下发帖和dispatcher模式发帖，发布帖子/评论/转发前都需要进行内容元数据的构建，构建的内容标准需要遵循 Metadata structure的定义，最终将json文件上传到**IPFS**作为content_uri的内容，为了保持统一，不需要上链同样需要进行这一步操作
     *
     * @tags Activity
     * @name AddActivity
     * @summary /activities
     * @request POST:/activities
     * @secure
     */
    addActivity: (data: AddActivityRequest, params: RequestParams = {}) =>
      this.request<ActivityResponse, any>({
        path: `/activities`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description get activity by id
     *
     * @tags Activity
     * @name GetActivity
     * @summary /activities/{activity_id}
     * @request GET:/activities/{activity_id}
     * @secure
     */
    getActivity: (activityId: string, params: RequestParams = {}) =>
      this.request<ActivityResponse, Response>({
        path: `/activities/${activityId}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags Activity
     * @name StopActivity
     * @summary /activities/{activity_id}
     * @request DELETE:/activities/{activity_id}
     * @secure
     */
    stopActivity: (activityId: string, params: RequestParams = {}) =>
      this.request<Response, Response>({
        path: `/activities/${activityId}`,
        method: "DELETE",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  feed = {
    /**
     * @description /feed/{feed_id}
     *
     * @tags Feed
     * @name GetFeed
     * @request GET:/feed/{feed_id}
     * @secure
     */
    getFeed: (
      feedId: string,
      query: {
        enrich_option: EnrichOption;
      },
      params: RequestParams = {}
    ) =>
      this.request<FeedResponse, any>({
        path: `/feed/${feedId}`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description /feed/{feed_type}/{feed_key}
     *
     * @tags Feed
     * @name ListFeed
     * @request GET:/feed/{feed_type}/{feed_key}
     * @secure
     */
    listFeed: (
      feedType: string,
      feedKey: string,
      query: {
        listFeedRequest: ListFeedRequest;
      },
      params: RequestParams = {}
    ) =>
      this.request<FeedPaginationResponse, any>({
        path: `/feed/${feedType}/${feedKey}`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),
  };
  profiles = {
    /**
     * No description
     *
     * @tags Profile
     * @name ListProfile
     * @summary /profiles
     * @request GET:/profiles
     * @secure
     */
    listProfile: (
      query?: {
        /** request info */
        profileRequest?: ListProfileRequest;
      },
      params: RequestParams = {}
    ) =>
      this.request<ProfilePaginationResponse, any>({
        path: `/profiles`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description get profile by id
     *
     * @tags Profile
     * @name GetProfileById
     * @summary /profiles/{profile_id}
     * @request GET:/profiles/{profile_id}
     * @secure
     */
    getProfileById: (profileId: string, params: RequestParams = {}) =>
      this.request<ProfileResponse, any>({
        path: `/profiles/${profileId}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description modify profile
     *
     * @tags Profile
     * @name UpdateProfile
     * @summary /profiles/{profile_id}
     * @request PUT:/profiles/{profile_id}
     * @secure
     */
    updateProfile: (
      profileId: string,
      query?: {
        /** update field & value */
        properties?: Record<string, object>;
      },
      params: RequestParams = {}
    ) =>
      this.request<ProfileResponse, any>({
        path: `/profiles/${profileId}`,
        method: "PUT",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description /profiles/{profile_id}/stories
     *
     * @tags Story
     * @name ListDynamic
     * @request GET:/profiles/{profile_id}/stories
     * @secure
     */
    listDynamic: (
      profileId: string,
      query: {
        /** 查询动态列表 */
        listDynamicRequest: ListStoryRequest;
      },
      params: RequestParams = {}
    ) =>
      this.request<StoryPaginationResponse, any>({
        path: `/profiles/${profileId}/stories`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags Profile
     * @name GetByHandle
     * @summary /profiles/handle/{profile_handle}
     * @request GET:/profiles/handle/{profile_handle}
     * @secure
     */
    getByHandle: (profileHandle: string, params: RequestParams = {}) =>
      this.request<ProfileResponse, any>({
        path: `/profiles/handle/${profileHandle}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description get own assets by profile
     *
     * @tags Asset
     * @name GetAssetsByProfileId
     * @summary /profiles/{profile_id}/assets
     * @request GET:/profiles/{profile_id}/assets
     * @secure
     */
    getAssetsByProfileId: (
      profileId: string,
      query?: {
        /** asset type, eg. nft,sbt */
        type?: AssetTypeEnum;
        /**
         * limit count
         * @example 10
         */
        limit?: number;
        /** next token for page */
        next_token?: string;
      },
      params: RequestParams = {}
    ) =>
      this.request<AssetPaginationResponse, any>({
        path: `/profiles/${profileId}/assets`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),
  };
  communities = {
    /**
     * @description query community by param
     *
     * @tags Community
     * @name ListCommunity
     * @summary /communities
     * @request GET:/communities
     * @secure
     */
    listCommunity: (
      query?: {
        /** request info */
        communityRequest?: ListCommunityRequest;
      },
      params: RequestParams = {}
    ) =>
      this.request<CommunityPaginationResponse, any>({
        path: `/communities`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description query community by id
     *
     * @tags Community
     * @name GetCommunityById
     * @summary /communities/{community_id}
     * @request GET:/communities/{community_id}
     * @secure
     */
    getCommunityById: (communityId: string, params: RequestParams = {}) =>
      this.request<CommunityResponse, any>({
        path: `/communities/${communityId}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags Community
     * @name UpdateCommunityById
     * @summary /communities/{community_id}
     * @request PUT:/communities/{community_id}
     * @secure
     */
    updateCommunityById: (
      communityId: string,
      data: UpdateCommunityRequest,
      params: RequestParams = {}
    ) =>
      this.request<CommunityResponse, any>({
        path: `/communities/${communityId}`,
        method: "PUT",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description query community by name
     *
     * @tags Community
     * @name GetCommunityByHandle
     * @summary /communities/handle/{community_handle}
     * @request GET:/communities/handle/{community_handle}
     * @secure
     */
    getCommunityByHandle: (
      communityHandle: string,
      params: RequestParams = {}
    ) =>
      this.request<CommunityResponse, any>({
        path: `/communities/handle/${communityHandle}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description update community domain by id
     *
     * @tags Community
     * @name UpdateDomainById
     * @summary /communities/{community_id}/domain
     * @request POST:/communities/{community_id}/domain
     * @secure
     */
    updateDomainById: (
      communityId: string,
      query: {
        /**
         * domain
         * @example "abc"
         */
        domain: string;
      },
      params: RequestParams = {}
    ) =>
      this.request<Response, any>({
        path: `/communities/${communityId}/domain`,
        method: "POST",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description query communities tags
     *
     * @tags Community
     * @name GetCommunityTags
     * @summary /communities/config/tags
     * @request GET:/communities/configs/tags
     * @secure
     */
    getCommunityTags: (params: RequestParams = {}) =>
      this.request<CommunityTagResponse, any>({
        path: `/communities/configs/tags`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  reactions = {
    /**
     * @description add reaction
     *
     * @tags Reaction
     * @name AddReaction
     * @summary /reactions
     * @request POST:/reactions
     * @secure
     */
    addReaction: (data: AddReactionRequest, params: RequestParams = {}) =>
      this.request<ReactionResponse, any>({
        path: `/reactions`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description get reaction by id
     *
     * @tags Reaction
     * @name GetReaction
     * @summary /reactions/{id}
     * @request GET:/reactions/{id}
     * @secure
     */
    getReaction: (id: string, params: RequestParams = {}) =>
      this.request<ReactionResponse, any>({
        path: `/reactions/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description delete reaction by id
     *
     * @tags Reaction
     * @name DeleteReaction
     * @summary /reactions/{id}
     * @request DELETE:/reactions/{id}
     * @secure
     */
    deleteReaction: (id: string, params: RequestParams = {}) =>
      this.request<Response, any>({
        path: `/reactions/${id}`,
        method: "DELETE",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags Reaction
     * @name GetReactions
     * @summary /reactions/{lookup_attr}/{lookup_value}/{kind}
     * @request GET:/reactions/{lookup_attr}/{lookup_value}/{kind}
     * @secure
     */
    getReactions: (
      lookupAttr: string,
      lookupValue: string,
      kind: ReactionKindEnum,
      query: {
        /** reaction request */
        listReactionRequest: ListReactionRequest;
      },
      params: RequestParams = {}
    ) =>
      this.request<ReactionPaginationResponse, any>({
        path: `/reactions/${lookupAttr}/${lookupValue}/${kind}`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags Reaction
     * @name DeleteReactionByKind
     * @summary /reactions/{lookup_attr}/{lookup_value}/{kind}
     * @request DELETE:/reactions/{lookup_attr}/{lookup_value}/{kind}
     * @secure
     */
    deleteReactionByKind: (
      lookupAttr: string,
      lookupValue: string,
      kind: ReactionKindEnum,
      params: RequestParams = {}
    ) =>
      this.request<Response, any>({
        path: `/reactions/${lookupAttr}/${lookupValue}/${kind}`,
        method: "DELETE",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  relations = {
    /**
     * No description
     *
     * @tags Relation
     * @name AddProfileFollow
     * @summary /relations/profiles/{profile_id}/following
     * @request POST:/relations/profiles/{profile_id}/following
     * @secure
     */
    addProfileFollow: (
      profileId: string,
      data: AddFollowingRequest,
      params: RequestParams = {}
    ) =>
      this.request<Response, any>({
        path: `/relations/profiles/${profileId}/following`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags Relation
     * @name DeleteProfileFollow
     * @summary /relations/profiles/{profile_id}/following
     * @request DELETE:/relations/profiles/{profile_id}/following
     * @secure
     */
    deleteProfileFollow: (profileId: string, params: RequestParams = {}) =>
      this.request<Response, any>({
        path: `/relations/profiles/${profileId}/following`,
        method: "DELETE",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags Relation
     * @name GetProfileFollowing
     * @summary /relations/profiles/{profile_id}/following
     * @request GET:/relations/profiles/{profile_id}/following
     * @secure
     */
    getProfileFollowing: (
      profileId: string,
      query?: {
        /** query request body */
        filter?: ListRelationRequest;
      },
      params: RequestParams = {}
    ) =>
      this.request<ProfilePaginationResponse, any>({
        path: `/relations/profiles/${profileId}/following`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags Relation
     * @name ListProfileFollowers
     * @summary /relations/profiles/{profile_id}/followers
     * @request GET:/relations/profiles/{profile_id}/followers
     * @secure
     */
    listProfileFollowers: (
      profileId: string,
      query?: {
        /** query request body */
        filter?: ListRelationRequest;
      },
      params: RequestParams = {}
    ) =>
      this.request<ProfilePaginationResponse, any>({
        path: `/relations/profiles/${profileId}/followers`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags Relation
     * @name GetProfileJoining
     * @summary /relations/profiles/{profile_id}/joining
     * @request GET:/relations/profiles/{profile_id}/joining
     * @secure
     */
    getProfileJoining: (
      profileId: string,
      query?: {
        /** query request body */
        filter?: ListRelationRequest;
      },
      params: RequestParams = {}
    ) =>
      this.request<CommunityPaginationResponse, any>({
        path: `/relations/profiles/${profileId}/joining`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags Relation
     * @name ListCommunityMember
     * @summary /relations/communities/{community_id}/members
     * @request GET:/relations/communities/{community_id}/members
     * @secure
     */
    listCommunityMember: (
      communityId: string,
      query?: {
        /** query request body */
        filter?: ListRelationRequest;
      },
      params: RequestParams = {}
    ) =>
      this.request<ProfilePaginationResponse, any>({
        path: `/relations/communities/${communityId}/members`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),
  };
  tx = {
    /**
     * @description ## 根据签名发送交易，测试使用
     *
     * @tags Transaction
     * @name TxBroadcast
     * @summary /broadcast
     * @request POST:/tx/broadcast
     * @secure
     */
    txBroadcast: (data: BroadcastRequest, params: RequestParams = {}) =>
      this.request<BroadcastResponse, any>({
        path: `/tx/broadcast`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description ## 可用于客户端先监听到数据或者补数据使用
     *
     * @tags Transaction
     * @name TxStore
     * @summary /tx/store
     * @request POST:/tx/store
     * @secure
     */
    txStore: (data: StoreRequest, params: RequestParams = {}) =>
      this.request<Response, any>({
        path: `/tx/store`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  typedata = {
    /**
     * No description
     *
     * @tags TypeData
     * @name AddProfileForTypeData
     * @summary /typedata/profile
     * @request POST:/typedata/profile
     * @secure
     */
    addProfileForTypeData: (data: ProfileRequest, params: RequestParams = {}) =>
      this.request<TypeDataResponse, any>({
        path: `/typedata/profile`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description add community for type data
     *
     * @tags TypeData
     * @name AddCommunityForTypeData
     * @summary /typedata/community
     * @request POST:/typedata/community
     * @secure
     */
    addCommunityForTypeData: (
      data: CommunityRequest,
      params: RequestParams = {}
    ) =>
      this.request<TypeDataResponse, any>({
        path: `/typedata/community`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags TypeData
     * @name AddActivityForTypeData
     * @summary /typedata/activity
     * @request POST:/typedata/activity
     * @secure
     */
    addActivityForTypeData: (
      data: ActivityRequest,
      params: RequestParams = {}
    ) =>
      this.request<TypeDataResponse, any>({
        path: `/typedata/activity`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags TypeData
     * @name UpdateDispatcherForTypeData
     * @summary /typedata/profile/dispatcher
     * @request PUT:/typedata/profile/dispatcher
     * @secure
     */
    updateDispatcherForTypeData: (
      data: {
        dispatcher: string;
        profile_id: string;
      },
      params: RequestParams = {}
    ) =>
      this.request<TypeDataResponse, any>({
        path: `/typedata/profile/dispatcher`,
        method: "PUT",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags TypeData
     * @name UpdateFollowModuleForTypeData
     * @summary /typedata/profile/follow_module
     * @request PUT:/typedata/profile/follow_module
     * @secure
     */
    updateFollowModuleForTypeData: (
      data: SetFollowModuleRequest,
      params: RequestParams = {}
    ) =>
      this.request<TypeDataResponse, any>({
        path: `/typedata/profile/follow_module`,
        method: "PUT",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags TypeData
     * @name UpdateJoinModuleForTypeData
     * @summary /typedata/profile/join_module
     * @request PUT:/typedata/community/join_module
     * @secure
     */
    updateJoinModuleForTypeData: (
      data: SetJoinModuleRequest,
      params: RequestParams = {}
    ) =>
      this.request<TypeDataResponse, any>({
        path: `/typedata/community/join_module`,
        method: "PUT",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description ### data传参数示例 - FreeFollowModule ```json {} ``` - FeeFollowModule ```json { "currency":"ETH", "value":"100000" } ``` - ProfileFollowModule ```json { "profileId":"0xxxx" } ``` - RevertFollowModule ```json {} ```
     *
     * @tags TypeData
     * @name AddFollowForTypeData
     * @summary /typedata/relation/follow
     * @request POST:/typedata/relation/follow
     * @secure
     */
    addFollowForTypeData: (data: FollowRequest, params: RequestParams = {}) =>
      this.request<TypeDataResponse, any>({
        path: `/typedata/relation/follow`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags TypeData
     * @name DeleteFollowForTypeData
     * @request DELETE:/typedata/relation/follow
     * @secure
     */
    deleteFollowForTypeData: (
      data: {
        target_profile_id?: string;
      },
      params: RequestParams = {}
    ) =>
      this.request<TypeDataResponse, any>({
        path: `/typedata/relation/follow`,
        method: "DELETE",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags TypeData
     * @name AddJoinForTypeData
     * @summary /typedata/relation/join
     * @request POST:/typedata/relation/join
     * @secure
     */
    addJoinForTypeData: (data: JoinRequest, params: RequestParams = {}) =>
      this.request<TypeDataResponse, any>({
        path: `/typedata/relation/join`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags TypeData
     * @name AddContentReactionForTypeData
     * @summary /typedata/reaction/content
     * @request POST:/typedata/reaction/content
     * @secure
     */
    addContentReactionForTypeData: (
      data: ContentReactionRequest,
      params: RequestParams = {}
    ) =>
      this.request<TypeDataResponse, any>({
        path: `/typedata/reaction/content`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags TypeData
     * @name AddReactionForTypeData
     * @summary /typedata/reaction
     * @request POST:/typedata/reaction
     * @secure
     */
    addReactionForTypeData: (
      data: ReactionRequest,
      params: RequestParams = {}
    ) =>
      this.request<TypeDataResponse, any>({
        path: `/typedata/reaction`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  mint = {
    /**
     * @description mint 1 ETH to user, only for test
     *
     * @tags Faucet
     * @name MintCoin
     * @summary /mint/coin
     * @request POST:/mint/coin
     * @secure
     */
    mintCoin: (
      data: {
        address: string;
      },
      params: RequestParams = {}
    ) =>
      this.request<TransactionResponse, any>({
        path: `/mint/coin`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description mint a slot nft for user to create community, only for test
     *
     * @tags Faucet
     * @name MintSlotNft
     * @summary /mint/slot_ntf
     * @request POST:/mint/slot_nft
     * @secure
     */
    mintSlotNft: (
      data: {
        address: string;
      },
      params: RequestParams = {}
    ) =>
      this.request<TransactionResponse, any>({
        path: `/mint/slot_nft`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description mint community whitelist creator, only for test
     *
     * @tags Faucet
     * @name MintCommunityWhitelistCreator
     * @summary /mint/community_whitelist_creator
     * @request POST:/mint/community_whitelist_creator
     * @secure
     */
    mintCommunityWhitelistCreator: (
      data: {
        address: string;
      },
      params: RequestParams = {}
    ) =>
      this.request<TransactionResponse, any>({
        path: `/mint/community_whitelist_creator`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description erc20 to user, only for test
     *
     * @tags Faucet
     * @name MintErc20
     * @summary /mint/erc20_token
     * @request POST:/mint/erc20_token
     * @secure
     */
    mintErc20: (
      data: {
        address: string;
        amount: string;
      },
      params: RequestParams = {}
    ) =>
      this.request<TransactionResponse, any>({
        path: `/mint/erc20_token`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  meta = {
    /**
     * @description get nft by token id
     *
     * @tags NFT
     * @name GetNftByTokenId
     * @summary /meta/{type}/{token_id}
     * @request GET:/meta/{type}/{token_id}
     * @secure
     */
    getNftByTokenId: (
      tokenId: string,
      type: BaseMetaTypeEnum,
      params: RequestParams = {}
    ) =>
      this.request<NFT, any>({
        path: `/meta/${type}/${tokenId}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description get join nft by token id, invoke by JoinNFT contract
     *
     * @tags NFT
     * @name GetJoinNftByCommunityIdAndTokenId
     * @summary /meta/{community_id}/join/{token_id}
     * @request GET:/meta/{community_id}/join/{token_id}
     * @secure
     */
    getJoinNftByCommunityIdAndTokenId: (
      communityId: string,
      tokenId: string,
      params: RequestParams = {}
    ) =>
      this.request<NFT, any>({
        path: `/meta/${communityId}/join/${tokenId}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description get join nft by token id, invoke by JoinNFT contract
     *
     * @tags NFT
     * @name GetFollowNftByProfileIdAndTokenId
     * @summary /meta/{profile_id}/follow/{token_id}
     * @request GET:/meta/{profile_id}/follow/{token_id}
     * @secure
     */
    getFollowNftByProfileIdAndTokenId: (
      profileId: string,
      tokenId: string,
      params: RequestParams = {}
    ) =>
      this.request<NFT, any>({
        path: `/meta/${profileId}/follow/${tokenId}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  contract = {
    /**
     * @description 获取OSP合约地址
     *
     * @tags Common
     * @name GetContractAddresses
     * @summary /contract/addresses
     * @request GET:/contract/addresses
     * @secure
     */
    getContractAddresses: (params: RequestParams = {}) =>
      this.request<ContractAddressResponse, any>({
        path: `/contract/addresses`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 获取OSP合约地址
     *
     * @tags Common
     * @name GetContractConfig
     * @summary /contract/config
     * @request GET:/contract/config
     * @secure
     */
    getContractConfig: (params: RequestParams = {}) =>
      this.request<ContractConfigResponse, any>({
        path: `/contract/config`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  ipfs = {
    /**
     * @description upload ipfs
     *
     * @tags Common
     * @name UploadIpfs
     * @summary /ipfs/upload
     * @request POST:/ipfs/upload
     * @secure
     */
    uploadIpfs: (
      data: {
        /** upload content body to ipfs for generate content uri cid */
        content?: string;
        /**
         * media file to upload
         * @format binary
         */
        media?: File;
      },
      params: RequestParams = {}
    ) =>
      this.request<Response, void>({
        path: `/ipfs/upload`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.FormData,
        format: "json",
        ...params,
      }),

    /**
     * @description pin ipfs file
     *
     * @tags Common
     * @name PinIpfs
     * @summary /ipfs/pin/{cid}
     * @request POST:/ipfs/pin/{cid}
     * @secure
     */
    pinIpfs: (cid: string, params: RequestParams = {}) =>
      this.request<Response, void>({
        path: `/ipfs/pin/${cid}`,
        method: "POST",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  erc4337 = {
    /**
     * @description check user operation
     *
     * @tags ERC4337
     * @name CheckPaymaster
     * @summary /erc4337/paymaster/check
     * @request GET:/erc4337/paymaster/check
     * @secure
     */
    checkPaymaster: (
      query: {
        sender: string;
        init_code: string;
        call_data: string;
      },
      params: RequestParams = {}
    ) =>
      this.request<PaymasterCheckResponse, any>({
        path: `/erc4337/paymaster/check`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description get paymaster and data by user operation
     *
     * @tags ERC4337
     * @name PaymasterData
     * @summary /erc4337/paymaster/data
     * @request POST:/erc4337/paymaster/data
     * @secure
     */
    paymasterData: (
      data: PaymasterAndDataRequest,
      params: RequestParams = {}
    ) =>
      this.request<PaymasterAndDataResponse, any>({
        path: `/erc4337/paymaster/data`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  plugin = {
    /**
     * @description query tokens by param ---can only query the logged-in user own
     *
     * @tags Plugin
     * @name GetTokens
     * @summary /tokens
     * @request GET:/plugin/tokens
     * @secure
     */
    getTokens: (
      query: {
        getTokenRequest: GetTokenRequest;
      },
      params: RequestParams = {}
    ) =>
      this.request<TokenPaginationResponse, any>({
        path: `/plugin/tokens`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description get token by id
     *
     * @tags Plugin
     * @name GetTokenById
     * @summary /tokens/{token_id}
     * @request GET:/plugin/tokens/{token_id}
     * @secure
     */
    getTokenById: (tokenId: string, params: RequestParams = {}) =>
      this.request<CoinTokenResponse, any>({
        path: `/plugin/tokens/${tokenId}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description modify token
     *
     * @tags Plugin
     * @name UpdateToken
     * @summary /tokens/{token_id}
     * @request PUT:/plugin/tokens/{token_id}
     * @secure
     */
    updateToken: (
      tokenId: string,
      data: UpdateTokenRequest,
      params: RequestParams = {}
    ) =>
      this.request<CoinTokenResponse, any>({
        path: `/plugin/tokens/${tokenId}`,
        method: "PUT",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description get token by contract address
     *
     * @tags Plugin
     * @name GetTokenByAddress
     * @summary /tokens/address/{address}
     * @request GET:/plugin/tokens/address/{address}
     * @secure
     */
    getTokenByAddress: (address: string, params: RequestParams = {}) =>
      this.request<CoinTokenResponse, any>({
        path: `/plugin/tokens/address/${address}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),
  };
}
