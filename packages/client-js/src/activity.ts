import { ethers } from "ethers";
import getUuid from "uuid-by-string";
import { Hex, hexToNumber } from "viem";
import { Client, DefaultGenerics } from "./client";
import {
  ACCOUNT_TYPE,
  APPID,
  OnChainTypeEnum,
  OspRes,
  SDK_ERROR,
} from "./constant";
import {
  ActivityCategoryEnum,
  ActivityRankingEnum,
  CollectModule,
  ReferenceModule,
} from "./rest_api_generated";
import { getMainContentFocus } from "./util/getMainContentFocus";
import { getEvent } from "./erc4337/receipt";

export type FeedData = {};

export type CollectType = {};

export type ReferenceType = {};

export type GetFeedOptions = {
  tenants?: any[];
  ranking: ActivityRankingEnum;
  randomize?: boolean;
  categories: ActivityCategoryEnum[];
  limit?: number;
  nextToken?: string;
};
interface FeedKindInterface {
  aggregated: string;
  timeline: string;
}
interface FeedTypeInterface {
  home: string;
  top: string;
  user_post: string;
}
export const FeedKind: FeedKindInterface = {
  aggregated: "aggregated",
  timeline: "timeline",
};

export const FeedType: FeedTypeInterface = {
  home: "community_home_post",
  top: "community_top_post",
  user_post: "user_post",
};

export class Activity<
  StreamFeedGenerics extends DefaultGenerics = DefaultGenerics
> {
  client: Client<StreamFeedGenerics>;
  constructor(client: Client<StreamFeedGenerics>) {
    this.client = client;
  }

  /**
   * addActivity
   * @link https://getstream.io/activity-feeds/docs/node/reactions_introduction/?language=js#adding-reactions
   * @method add
   * @memberof StreamReaction.prototype
   * @param  {string}   kind  kind of reaction
   * @param  {string}   activity Activity or an ActivityID
   * @param  {ReactionType}   data  data related to reaction
   * @param  {ReactionAddOptions} [options]
   * @param  {string} [options.id] id associated with reaction
   * @param  {string[]} [options.targetFeeds] an array of feeds to which to send an activity with the reaction
   * @param  {object} [options.targetFeedsExtraData] extra data related to target feeds
   * @return {Promise<ReactionAPIResponse<ReactionType>>}
   * @example reactions.add("like", "0c7db91c-67f9-11e8-bcd9-fe00a9219401")
   * @example reactions.add("comment", "0c7db91c-67f9-11e8-bcd9-fe00a9219401", {"text": "love it!"},)
   */
  async addActivity(
    {
      communityId,
      metadata,
      collect,
      reference,
      isOnChain,
      onChainType,
    }: {
      communityId: Hex;
      metadata?: any;
      collect?: CollectModule;
      reference?: ReferenceModule;
      isOnChain: boolean;
      onChainType?: OnChainTypeEnum;
    },
    onCompleted = (id: string) => {}
  ) {
    const _metadata: any = {
      version: "2.0.0",
      metadata_id: getUuid(JSON.stringify(metadata)),
      external_url: `https://lenster.xyz/u/${this.client.profile.handle}`,
      image: null,
      imageMimeType: null,
      name: `Post by ${this.client.profile.handle}`,
      tags: [],
      animation_url: null,
      mainContentFocus: getMainContentFocus(metadata),
      contentWarning: null,
      attributes: [
        {
          traitType: "type",
          displayType: "string",
          value: getMainContentFocus(metadata),
        },
      ],
      medias: [],
      locale: "zh-CN",
      appId: APPID,
      ...metadata,
    };
    if (metadata?.image) {
      _metadata.image = metadata?.image.map(
        (each: [string, string]) => each[0]
      );
      _metadata.medias = metadata?.image.map((each: [string, string]) => {
        return { url: each[0], mime_type: `image/${each[1]}` };
      });
    }
    let content_uri;
    try {
      content_uri = await this.client.ipfs.upload(JSON.stringify(_metadata));
    } catch (e) {
      return { code: SDK_ERROR.IPFS_ERROR, ...e };
    }

    if (isOnChain) {
      return await this.createActivityWithChain(
        {
          communityId,
          content_uri,
          metadata: _metadata,
          onChainType,
        },
        onCompleted
      );
    } else {
      const res = await this.addActivityRest({
        communityId,
        content_uri,
        metadata: _metadata,
      });
      onCompleted(null);
      return res;
    }
  }

  async addActivityRest({
    communityId,
    content_uri,
    metadata,
  }: {
    communityId: string;
    metadata: any;
    content_uri: string;
  }): Promise<OspRes> {
    const res = await this.client.request.call(["activities", "addActivity"], {
      community_id: communityId,
      content_uri,
      content:
        typeof metadata !== "string" ? JSON.stringify(metadata) : metadata,
    });
    return res;
  }

  async createActivityWithChain(
    {
      communityId,
      content_uri,
      metadata,
      onChainType,
    }: {
      content_uri: string;
      communityId: Hex;
      metadata: any;
      accountType?: ACCOUNT_TYPE;
      onChainType?: OnChainTypeEnum;
    },
    onCompleted = (contentId: string) => {}
  ) {
    let callData = this.client.wallet.ospClient.interface.encodeFunctionData(
      "post",
      [
        {
          profileId: BigInt(hexToNumber(this.client.profile.profileId as Hex)),
          communityId: BigInt(hexToNumber(communityId)),
          contentURI: content_uri,
          collectModule: this.client.wallet.contractConfig.free_collect_module,
          collectModuleInitData: ethers.utils.defaultAbiCoder.encode(
            ["bool"],
            [true]
          ),
          referenceModule: "0x0000000000000000000000000000000000000000",
          referenceModuleInitData: "0x",
        },
      ]
    ) as Hex;
    return this.client.wallet.account
      .sendOpAndGetResult({ data: callData }, (receipt) => {
        onCompleted(
          ethers.utils.hexValue(getEvent("PostCreated", receipt)["contentId"])
        );
      })
      .then(async (res) => {
        if (res.error) {
          return { data: null, error: res.error };
        }
        if (onChainType === OnChainTypeEnum.ON_CHAIN) {
          let res2 = await this.addActivityRest({
            communityId,
            content_uri,
            metadata,
          });
          return { data: { ...res.data, ...res2.data }, error: null };
        }

        return { data: { ...res.data }, error: null };
      });
  }

  /**
   * Reads the feed
   * @link https://getstream.io/activity-feeds/docs/node/adding_activities/?language=js#retrieving-activities
   * @method get
   * @memberof StreamFeed.prototype
   * @param {GetFeedOptions} options  Additional options
   * @return {Promise<FeedAPIResponse>}
   * @example feed.get({limit: 10, id_lte: 'activity-id'})
   * @example feed.get({limit: 10, mark_seen: true})
   */
  async stories(profileId: string, query: {}) {
    const res = await this.client.request.call(
      ["profiles", "listDynamic"],
      profileId,
      query
    );
    return res;
  }

  async feeds(
    kind = "aggregated",
    type = "home",
    query = { communityId: "", profileId: "" }
  ) {
    const enrichOption: any = {
      enrich: true,
      withRecentReactions: true,
      recentReactionsLimit: 20,
      withOwnReactions: true,
      withOwnChildren: true,
      withReactionCounts: true,
      reactionKindsFilter: ["COMMENT"],
      with_profile: false,
      with_community: false,
      with_relations: ["JOIN"],
    };
    const params: any = {
      a_community: query?.communityId,
      a_profile: query?.profileId,
    };
    const obj: any = {};
    const obj1: any = {};
    Object.keys(enrichOption).forEach(
      (val) => (obj[`enrichOption.${val}`] = enrichOption[val])
    );
    Object.keys(params).forEach(
      (val) => (obj1[`params[${val}]`] = params[val])
    );

    const res = await this.client.request.call(
      ["feed", "listFeed"],
      FeedKind[kind as keyof FeedKindInterface],
      FeedType[type as keyof FeedTypeInterface],
      {
        ...obj,
        ...obj1,

        limit: 10,
        reverse: true,
        next_token: "",
        exclude_view_ids: [""],
        ranking: "ACTIVITY_TIME",
        mark_read: true,
        ...query,
      }
    );
    return res;
  }

  async getFeedDetail(feedId: string, query = {}) {
    const res = await this.client.request.call(
      ["feed", "getFeed"],
      feedId,
      query
    );
    return res;
  }
}
