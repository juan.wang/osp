import { PublicationMainFocus } from "../type";

export const getMainContentFocus = (metadata?: any) => {
  if (metadata.image?.length > 0) {
    return PublicationMainFocus.Image;
  } else {
    return PublicationMainFocus.TextOnly;
  }
};
