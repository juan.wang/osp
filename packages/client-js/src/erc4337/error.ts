export class GasNotSufficient extends Error {
  public balance: string;
  public requiredPrefund: string;
  public symbol: string;
  constructor(balance: string, requiredPrefund: string, symbol: string) {
    super();
    this.balance = balance;
    this.requiredPrefund = requiredPrefund;
    this.symbol = symbol;
  }
}
