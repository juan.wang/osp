import {
  AccountMiddlewareFn,
  Address,
  deepHexlify,
  PublicErc4337Client,
  resolveProperties,
  SmartAccountProvider,
  SmartAccountProviderOpts,
  SupportedTransports,
  UserOperationStruct,
} from "@alchemy/aa-core";

import { Chain } from "viem";
import { IPaymaster } from "./paymaster";
import { BigNumber, ethers } from "ethers";
import { SmartAccount } from "./account";
import { GasNotSufficient } from "./error";

export class AAProvider<
  TTransport extends SupportedTransports
> extends SmartAccountProvider<TTransport> {
  paymaster?: IPaymaster;
  constructor(
    rpcProvider: string | PublicErc4337Client<TTransport>,
    protected entryPointAddress: Address,
    protected chain: Chain,
    paymaster?: IPaymaster,
    readonly account?: SmartAccount,
    opts?: SmartAccountProviderOpts
  ) {
    super(rpcProvider, entryPointAddress, chain, account, opts);
    this.paymaster = paymaster;
  }

  async getBalance(): Promise<BigNumber> {
    return this.account.getBalance();
  }
  // These are dependent on the specific paymaster being used
  // You should implement your own middleware to override these
  // or extend this class and provider your own implemenation
  readonly dummyPaymasterDataMiddleware: AccountMiddlewareFn = async (
    struct: UserOperationStruct
  ): Promise<UserOperationStruct> => {
    if (this.paymaster)
      return this.paymaster.dummyPaymasterDataMiddleware(struct);
    struct.paymasterAndData = "0x";
    return struct;
  };

  readonly paymasterDataMiddleware: AccountMiddlewareFn = async (
    struct: UserOperationStruct
  ): Promise<UserOperationStruct> => {
    if (this.paymaster) return this.paymaster.paymasterDataMiddleware(struct);
    struct.paymasterAndData = "0x";
    return struct;
  };

  readonly gasEstimator: AccountMiddlewareFn = async (struct) => {
    struct.callGasLimit = "0x10";
    struct.maxFeePerGas = "0x10";
    struct.maxPriorityFeePerGas = "0x10";
    struct.preVerificationGas = "0x10";
    struct.verificationGasLimit = "0x10";
    const request = deepHexlify(await resolveProperties(struct));
    const estimates = await this.rpcClient.estimateUserOperationGas(
      request,
      this.entryPointAddress
    );
    const verificationGasLimit =
      BigInt(estimates.verificationGasLimit) + 21000n;
    struct.callGasLimit = estimates.callGasLimit;
    struct.preVerificationGas = BigInt(estimates.preVerificationGas) + 2000n;
    struct.verificationGasLimit = verificationGasLimit;
    return struct;
  };

  readonly feeDataGetter: AccountMiddlewareFn = async (struct) => {
    // const maxPriorityFeePerGas = await this.rpcClient.getMaxPriorityFeePerGas();
    const feeData = await this.rpcClient.getFeeData();
    // console.log(maxPriorityFeePerGas);
    console.log(feeData);
    if (!feeData.maxFeePerGas || !feeData.maxPriorityFeePerGas) {
      throw new Error(
        "feeData is missing maxFeePerGas or maxPriorityFeePerGas"
      );
    }

    // add 33% to the priorty fee to ensure the transaction is mined
    let maxPriorityFeePerGasBid =
      (BigInt(feeData.maxPriorityFeePerGas) * 4n) / 3n;
    if (maxPriorityFeePerGasBid < this.minPriorityFeePerBid) {
      maxPriorityFeePerGasBid = this.minPriorityFeePerBid;
    }

    const maxFeePerGasBid =
      BigInt(feeData.maxFeePerGas) -
      BigInt(feeData.maxPriorityFeePerGas) +
      maxPriorityFeePerGasBid;

    struct.maxFeePerGas = maxFeePerGasBid;
    struct.maxPriorityFeePerGas = maxPriorityFeePerGasBid;
    let requiredPrefund =
      (BigInt(await struct.callGasLimit) +
        BigInt(await struct.verificationGasLimit) +
        BigInt(await struct.preVerificationGas)) *
      BigInt(await struct.maxFeePerGas);
    const balance = await this.getBalance();
    if (
      (await struct.paymasterAndData) == "0x" &&
      balance.lt(requiredPrefund)
    ) {
      throw new GasNotSufficient(
        ethers.utils.formatUnits(balance, this.chain.nativeCurrency.decimals),
        ethers.utils.formatUnits(
          requiredPrefund,
          this.chain.nativeCurrency.decimals
        ),
        this.chain.nativeCurrency.symbol
      );
    }
    return struct;
  };
}
