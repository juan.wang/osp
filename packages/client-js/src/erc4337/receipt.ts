import { EventFragment } from "@ethersproject/abi";
import { Log } from "@ethersproject/abstract-provider";
import { OspEvents__factory } from "typechain-types";
import { TransactionReceipt } from "@ethersproject/abstract-provider";
import { UserOperationReceiptObject } from "@alchemy/aa-core";

const OSP_EVENTS = OspEvents__factory.createInterface();

export interface Receipt {
  txHash: string;
  logs: Array<Log>;
}

export function fromTransactionReceipt(receipt: TransactionReceipt): Receipt {
  return { txHash: receipt.transactionHash, logs: receipt.logs };
}

export function fromUserOperationReceipt(
  receipt: UserOperationReceiptObject
): Receipt {
  return {
    txHash: receipt.transactionHash,
    logs: receipt.logs.map((log) => {
      return {
        ...log,
        blockNumber: Number(log.blockNumber),
        transactionIndex: Number(log.transactionIndex),
        logIndex: Number(log.logIndex),
      };
    }),
  };
}

export function getEvent(
  eventFragment: EventFragment | string,
  receipt: Receipt
) {
  if (typeof eventFragment === "string") {
    // @ts-ignore
    eventFragment = OSP_EVENTS.getEvent(eventFragment);
  }
  const topic0 = OSP_EVENTS.getEventTopic(eventFragment);
  return receipt.logs
    .filter((i) => i.topics[0] == topic0)
    .map((i) => OSP_EVENTS.decodeEventLog(eventFragment, i.data, i.topics))[0];
}
