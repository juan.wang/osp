import { WALLET_ADAPTERS, WALLET_ADAPTER_TYPE } from "@web3auth/base";
import { OpenloginLoginParams } from "@web3auth/openlogin-adapter";
import { Client, DefaultGenerics } from "./client";
import { WALLET_ERROR, WALLET_TYPE } from "./constant";

export class Auth<
  StreamFeedGenerics extends DefaultGenerics = DefaultGenerics
> {
  client: Client<StreamFeedGenerics>;
  interval: any;
  constructor(client: Client<StreamFeedGenerics>) {
    this.client = client;
    const access_token = client.cache?.getItem("sdk:access_token");
    const refresh_token = client.cache?.getItem("sdk:refresh_token");
    if (access_token) {
      this.setReqHeaderToken(access_token);
    }
    if (refresh_token) {
      this.startRefreshInterval();
    }
  }

  cacheToken(access_token: string, refresh_token: string) {
    if (this.client.enableCache) {
      this.client.cache?.setItem("sdk:access_token", access_token);
      this.client.cache?.setItem("sdk:refresh_token", refresh_token);
    }
  }

  startRefreshInterval() {
    if (this.interval) return;
    this.interval = setInterval(async () => {
      try {
        const _refresh_token = this.client.cache?.getItem("sdk:refresh_token");
        const {
          data: { access_token, refresh_token },
          error,
        } = await this.client.request.call(["auth", "refreshToken"], {
          refresh_token: _refresh_token,
        });
        this.setReqHeaderToken(access_token);
        this.cacheToken(access_token, refresh_token);
      } catch (e) {}
    }, 1000 * 60 * 10);
  }

  finishRefreshInterval() {
    clearInterval(this.interval);
    this.interval = null;
  }

  /**
   * signIn
   * @link https://getstream.io/activity-feeds/docs/node/reactions_introduction/?language=js#adding-reactions
   * @method add
   * @memberof StreamReaction.prototype
   * @param  {string}   kind  kind of reaction
   * @param  {string}   activity Activity or an ActivityID
   * @param  {ReactionType}   data  data related to reaction
   * @param  {ReactionAddOptions} [options]
   * @param  {string} [options.id] id associated with reaction
   * @param  {string[]} [options.targetFeeds] an array of feeds to which to send an activity with the reaction
   * @param  {string} [options.userId] useful for adding reaction with server token
   * @param  {object} [options.targetFeedsExtraData] extra data related to target feeds
   * @return {Promise<ReactionAPIResponse<ReactionType>>}
   * @example reactions.add("like", "0c7db91c-67f9-11e8-bcd9-fe00a9219401")
   * @example reactions.add("comment", "0c7db91c-67f9-11e8-bcd9-fe00a9219401", {"text": "love it!"},)
   */
  setReqHeaderToken(access_token: string) {
    this.client.request.api?.setSecurityData({
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
    });
  }
  async signIn(wallet_type: WALLET_TYPE = WALLET_TYPE.METAMASK) {
    let walletName: WALLET_ADAPTER_TYPE;
    let loginParams: OpenloginLoginParams;
    const redirectUrl = "http://www.osp.com";
    if (wallet_type === WALLET_TYPE.METAMASK) {
      walletName = WALLET_ADAPTERS.METAMASK;
    } else if (wallet_type === WALLET_TYPE.GOOGLE) {
      walletName = WALLET_ADAPTERS.OPENLOGIN;
      loginParams = { loginProvider: "google", redirectUrl };
    } else if (wallet_type === WALLET_TYPE.FACEBOOK) {
      walletName = WALLET_ADAPTERS.OPENLOGIN;
      loginParams = { loginProvider: "facebook", redirectUrl };
    } else if (wallet_type === WALLET_TYPE.TWITTER) {
      walletName = WALLET_ADAPTERS.OPENLOGIN;
      loginParams = { loginProvider: "twitter", redirectUrl };
    } else {
      return { error: { code: WALLET_ERROR.WALLET_TYPE_NOT_SUPPORT } };
    }
    let connResult = await this.client.wallet.connect(walletName, loginParams);
    if (connResult?.error) return connResult;
    let { idToken } = await this.client.wallet.web3Auth.authenticateUser();
    if (!idToken) return { error: "no idToken" };
    const { error: errorAuth, data: dataAuth }: any =
      await this.client.request.call(["auth", "authenticate"], {
        address: this.client.wallet.eoaAddress,
        signature: idToken,
      });
    if (errorAuth) {
      return { error: errorAuth };
    }
    const { access_token, refresh_token } = dataAuth;

    this.setReqHeaderToken(access_token);
    this.cacheToken(access_token, refresh_token);
    this.startRefreshInterval();
    //目前SDK只支持AA账号 所以先根据AA账号获取profile
    const { data: userProfile, error: errorProfiles } =
      await this.client.profile.getAllProfile({
        address: this.client.wallet.account.address,
      });
    if (this.client.enableCache) {
      if (userProfile?.length) {
        const { profile_id } = userProfile[0];
        const obj = {} as any;
        obj[profile_id] = userProfile[0];
        console.log(obj);
        await this.client.cache?.setItem("sdk:user", JSON.stringify(obj));
        this.client.profile.setCurUserInfo();
      }
    }
    return { data: { profiles: userProfile }, error: errorProfiles };
  }

  async signOut() {
    await this.client.wallet.logout();
    [
      "sdk:access_token",
      "sdk:refresh_token",
      "sdk:address",
      "sdk:user",
    ].forEach((item) => this.client.cache?.removeItem(item));
    this.setReqHeaderToken("");
    this.finishRefreshInterval();
  }
}
