import { Client } from "../client";
import { BigNumber, BigNumberish, ethers } from "ethers";
import { Wallet } from "../wallet";
import { CoinMintPlugin__factory, ERC20__factory } from "typechain-types";
import { getEvent } from "../erc4337/receipt";
import { UserOp } from "../erc4337/osp-account";
import { OspRes, WALLET_ERROR } from "../constant";
import { CoinTokenResponse, GetTokenRequest } from "../rest_api_generated";

interface CreateCoinOnChainData {
  name: string;
  symbol: string;
  decimals: number;
  initSupply: BigNumberish;
  claimSupply: BigNumberish;
  claimLimitPreAddress: BigNumberish;
  claimStartTime: BigNumberish;
  claimEndTime: BigNumberish;
  claimPriceRange: BigNumberish[];
  claimPrice: BigNumberish[];
  claimPayCurrencyAddress: string;
  communityIds: BigNumberish[];
}

interface UpdateCoinOffChainData {
  remark: string;
  distribution: string;
  banner: string;
}
const coinMintPluginInterface = CoinMintPlugin__factory.createInterface();

export class CoinMintPlugin {
  client: Client;

  constructor(client: Client) {
    this.client = client;
  }

  async createCoinOnChain(
    createCoinData: CreateCoinOnChainData,
    onCompleted = (tokenAddress: string, token: CoinTokenResponse) => {}
  ) {
    const wallet: Wallet = this.client.wallet;
    const callData = coinMintPluginInterface.encodeFunctionData("createToken", [
      createCoinData,
    ]);
    const res = await wallet.account.sendOpAndGetResult(
      { data: callData, to: wallet.contractConfig.coin_mint_plugin_address },
      async (receipt) => {
        const coinCreatEvent = getEvent(
          coinMintPluginInterface.getEvent("CoinCreate"),
          receipt
        );
        const tokenAddress = coinCreatEvent.tokenAddress;
        const res = await this.client.request.call(
          ["plugin", "getTokenByAddress"],
          tokenAddress
        );
        console.log(res);
        onCompleted(tokenAddress, res);
      }
    );
    return res;
  }

  async claimCoinOnChain(
    tokenAddress: string,
    communityId: BigNumberish,
    amount: BigNumberish,
    onCompleted = () => {}
  ): Promise<OspRes> {
    const wallet = this.client.wallet;
    amount = BigNumber.from(amount);
    if (
      (
        await wallet.coinMintPlugin.isClaimed(wallet.account.address, [
          tokenAddress,
        ])
      )[0]
    ) {
      return { data: null, error: { code: "ALREADY_CLAIMED" } };
    }
    const coinData = await wallet.coinMintPlugin.getCoinData(tokenAddress);
    const userOps: UserOp[] = [];
    let nativeValue = 0n;
    const claimRanking = coinData.coinData.claimAddressCount.toBigInt() + 1n;
    const claimPriceRange = coinData.coinData.claimPriceRange;
    const claimPrice = coinData.coinData.claimPrice;
    let index = 0;
    for (; index < claimPriceRange.length; index++) {
      if (claimPriceRange[index].gt(claimRanking)) {
        break;
      }
    }
    const price = index == 0 ? BigNumber.from(0) : claimPrice[index - 1];
    if (!price.isZero()) {
      const cost = price
        .mul(amount)
        .div(BigNumber.from(10).pow(coinData.decimals));
      if (
        coinData.coinData.claimPayCurrencyAddress ===
        ethers.constants.AddressZero
      ) {
        if (cost.gt(await wallet.account.getBalance())) {
          return { error: { code: WALLET_ERROR.WALLET_BALANCE_INSUFFICIENT } };
        }
        nativeValue = cost.toBigInt();
      } else {
        const paymentToken = ERC20__factory.connect(
          coinData.coinData.claimPayCurrencyAddress,
          wallet.etherProvider
        );
        if (cost.gt(await paymentToken.balanceOf(wallet.account.address))) {
          return { error: { code: WALLET_ERROR.WALLET_BALANCE_INSUFFICIENT } };
        }
        if (
          cost.gt(
            await paymentToken.allowance(
              wallet.account.address,
              wallet.contractConfig.coin_mint_plugin_address
            )
          )
        ) {
          const approveCallData = paymentToken.interface.encodeFunctionData(
            "approve",
            [wallet.contractConfig.coin_mint_plugin_address, cost]
          );
          userOps.push({ data: approveCallData, to: paymentToken.address });
        }
      }
    }
    userOps.push({
      data: coinMintPluginInterface.encodeFunctionData("claimToken", [
        tokenAddress,
        communityId,
        amount,
      ]),
      to: wallet.contractConfig.coin_mint_plugin_address,
      value: nativeValue,
    });
    return await wallet.account.sendOpAndGetResult(userOps, onCompleted);
  }

  async updateCoinOffChain(
    tokenId: string,
    updateCoinData: UpdateCoinOffChainData
  ) {
    return await this.client.request.call(["plugin", "updateToken"], tokenId, {
      fields: updateCoinData,
    });
  }

  async getAllTokens(getTokenRequest: GetTokenRequest) {
    const res = await this.client.request.call(
      ["plugin", "getTokens"],
      getTokenRequest
    );
    return res;
  }

  async getToken(tokenId: string) {
    const res = await this.client.request.call(
      ["plugin", "getTokenById"],
      tokenId
    );
    return res;
  }
}
