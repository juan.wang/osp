import { DefaultGenerics, Client } from "./client";

export class IPFS<
  StreamFeedGenerics extends DefaultGenerics = DefaultGenerics
> {
  client: Client<StreamFeedGenerics>;

  constructor(client: Client<StreamFeedGenerics>) {
    this.client = client;
  }

  async upload(data: unknown) {
    const res = await this.client.request.call(
      ["ipfs", "uploadIpfs"],
      typeof data === "string" ? { content: data } : { media: data }
    );
    return `ipfs://${res.data}`;
  }
}
