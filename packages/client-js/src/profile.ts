import { Hex } from "viem";
import { hexToNumber } from "web3-utils";
import { Client, DefaultGenerics } from "./client";
import { getJoinCallData } from "./community";
import { OspRes } from "./constant";
import { BatchUserOp } from "./erc4337/osp-account";
import {
  FollowModule,
  FollowModuleEnum,
  ListProfileRequest,
} from "./rest_api_generated";
import { getEvent } from "./erc4337/receipt";
import { ethers } from "ethers";

export type QueryParamsType = {
  /** asset type, eg. NFT,SBT */
  type?: string;
  /** limit count */
  limit?: number;
  /** next token for page */
  next_token?: string;
};
export class Profile<
  StreamFeedGenerics extends DefaultGenerics = DefaultGenerics
> {
  client: Client<StreamFeedGenerics>;
  profileId: string;
  handle: string;
  constructor(client: Client<StreamFeedGenerics>) {
    this.client = client;
    this.setCurUserInfo();
  }

  async setCurUserInfo() {
    const user = await this.client.cache?.getItem("sdk:user");
    let userObj = {} as any;
    try {
      userObj = JSON.parse(user);
      const curUserKey = Object.keys(userObj || {})[0] || "";
      if (curUserKey) {
        this.profileId = userObj[curUserKey].profile_id;
        this.handle = userObj[curUserKey].handle;
      }
    } catch (err) {}
  }

  /**
   * Delete the user
   * create profile you need to pass in the type of account you want to create, default is AA
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#removing-users
   * @return {Promise<APIResponse>}
   */
  async create(
    creationData: {
      handle: string;
      followModule?: FollowModule;
    },
    communityId?: string,
    onCompleted = (profileId: string) => {}
  ): Promise<OspRes> {
    if (!creationData.followModule) {
      creationData.followModule = {
        type: FollowModuleEnum.NULL_FOLLOW_MODULE,
        init: {
          null_follow_module: {},
        },
      };
    }
    //目前followModule还没有其他类型，先不做处理
    let profileCallData =
      this.client.wallet.ospClient.interface.encodeFunctionData(
        "createProfile",
        [
          {
            handle: creationData.handle,
            followModule: "0x0000000000000000000000000000000000000000",
            followModuleInitData: "0x",
          },
        ]
      ) as Hex;

    let ops: BatchUserOp = [{ data: profileCallData }];
    if (communityId) {
      try {
        let batchUserOp = await getJoinCallData(
          this.client,
          BigInt(hexToNumber(communityId))
        );
        ops.push(...batchUserOp);
      } catch (e) {
        return {
          data: null,
          error: {
            code: e.message,
          },
        };
      }
    }
    return this.client.wallet.account.sendOpAndGetResult(ops, (receipt) => {
      onCompleted(
        ethers.utils.hexValue(getEvent("ProfileCreated", receipt)["profileId"])
      );
    });
  }
  /**
   * Update a new user in stream
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#adding-users
   * @param {object} data user date stored in stream
   * @param {boolean} [options.get_or_create] if user already exists return it
   * @return {Promise<StreamUser>}
   */
  async update(
    profileId: string,
    info: { [key: string]: any }
  ): Promise<OspRes> {
    const res = await this.client.request.call(
      ["profiles", "updateProfile"],
      profileId,
      info
    );

    return res;
  }

  /**
   * Get the user profile
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#retrieving-users
   * @param {boolean} [options.with_follow_counts]
   * @return {Promise<StreamUser>}
   */
  async get(profileId: string): Promise<OspRes> {
    const res = await this.client.request.call(
      ["profiles", "getProfileById"],
      profileId
    );
    return res;
  }
  /**
   * Get the user profile
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#retrieving-users
   * @param {boolean} [options.with_follow_counts]
   * @return {Promise<StreamUser>}
   */
  async getByHandle(handle: string): Promise<OspRes> {
    const res = await this.client.request.call(
      ["profiles", "getByHandle"],
      handle
    );
    return res;
  }
  /**
   * Get the user profile
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#retrieving-users
   * @param {boolean} [options.with_follow_counts]
   * @return {Promise<StreamUser>}
   */
  async getAsset(profileId: string, query: QueryParamsType): Promise<OspRes> {
    const res = await this.client.request.call(
      ["profiles", "getAssetsByProfileId"],
      profileId,
      query
    );
    return res;
  }

  /**
   * Get the user all profile
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#retrieving-users
   * @param {boolean} [options.with_follow_counts]
   * @return {Promise<StreamUser>}
   */
  async getAllProfile(query: ListProfileRequest): Promise<OspRes> {
    const { data, error } = await this.client.request.call(
      ["profiles", "listProfile"],
      query
    );
    if (error) return { error };
    const rows = data.rows;
    return { data: rows, error: null };
  }
}
