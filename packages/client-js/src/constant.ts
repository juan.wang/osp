import { CHAIN_NAMESPACES, CustomChainConfig } from "@web3auth/base";
import { Chain, polygonMumbai, sepolia } from "viem/chains";

export const APPID = "OSP";
//TO-DO
export const clientId =
  "BOlvW9_5mCDeBQ6hFrh8lyPZ-ReoBGEXnQjWj6-l_8g-VM1TqEHNaYEXAvK5Af15uPVgMzJREdWzUttbO9tcuAs";

export const WALLET_ERROR = {
  NO_WALLET: "NO_WALLET",
  NO_INSTALL_METAMEASK: "NO_INSTALL_METAMEASK",
  NO_CONNECT_METAMEASK: "NO_CONNECT_METAMEASK",
  "-32603": "USER_REJECT",
  "-32602": "PARAMS_INVALID",
  "4001": "USER_REJECT",
  "4100": "UNAUTHORIZED",
  "4200": "UNSUPPORTED_METHOD",
  "4900": "DISCONNECTED",
  "4901": "CHAIN_DISCONNECTED",
  WALLET_LOCKED: "WALLET_LOCKED",
  WALLET_ACCOUNT_CHANGED: "WALLET_ACCOUNT_CHANGED",
  WALLET_DISCONNECT: "WALLET_DISCONNECT",
  WALLET_CHAIN_CHANGED: "WALLET_CHAIN_CHANGED",
  WALLET_CONTRACT_INIT_ERROR: "WALLET_CONTRACT_INIT_ERROR",
  WALLET_TYPE_NOT_SUPPORT: "WALLET_TYPE_NOT_SUPPORT",
  WALLET_BALANCE_INSUFFICIENT: "WALLET_BALANCE_INSUFFICIENT",
  GAS_NOT_SUFFICIENT: "GAS_NOT_SUFFICIENT",
  UNKNOWN_ERROR: "UNKNOWN_ERROR",
};

export const SDK_ERROR = {
  IPFS_ERROR: "IPFS_ERROR",
};

export enum WALLET_TYPE {
  METAMASK,
  GOOGLE,
  FACEBOOK,
  TWITTER,
}

export enum OnChainTypeEnum {
  ONLY_ON_CHAIN = "ONLY_ON_CHAIN",
  ON_CHAIN = "ON_CHAIN",
}

export const CHAIN_CONFIG: { [name: string]: CustomChainConfig } = {
  local: {
    chainNamespace: CHAIN_NAMESPACES.EIP155,
    rpcTarget:
      "https://eth-sepolia.g.alchemy.com/v2/UJu10EBnVKSI-qO8tp4yarUqVAoNAcrV",
    blockExplorer: "https://sepolia.etherscan.io/",
    chainId: "0xaa36a7",
    displayName: "SepoliaTest",
    ticker: "ETH",
    tickerName: "ETH",
  },
  dev: {
    chainNamespace: CHAIN_NAMESPACES.EIP155,
    rpcTarget:
      "https://eth-sepolia.g.alchemy.com/v2/UJu10EBnVKSI-qO8tp4yarUqVAoNAcrV",
    blockExplorer: "https://sepolia.etherscan.io/",
    chainId: "0xaa36a7",
    displayName: "SepoliaTest",
    ticker: "ETH",
    tickerName: "ETH",
  },
  beta: {
    displayName: "Mumbai",
    chainNamespace: CHAIN_NAMESPACES.EIP155,
    chainId: "0x13881",
    rpcTarget: `https://api.zan.top/node/v1/polygon/mumbai/38c248ede39c4a1181a9d8df0168e6c9`,
    blockExplorer: "https://mumbai.polygonscan.com",
    ticker: "MATIC",
    tickerName: "Mumbai",
  },
};

export const CHAIN_MAP: { [name: string]: Chain } = {
  "11155111": sepolia,
  "80001": polygonMumbai,
};

export enum ACCOUNT_TYPE {
  AA,
  EOA,
}

export const CONTRACT_REVERT_MAP: { [name: string]: string } = {
  "0xb7c1140d": "ArrayMismatch",
  "0x2b23a7c0": "BlockNumberInvalid",
  "0xc6d1651b": "CallerNotCollectNFT",
  "0x19e7cdac": "CallerNotFollowSBT",
  "0x69fa2403": "CallerNotWhitelistedModule",
  "0x971d0414": "CannotInitImplementation",
  "0x99ae0720": "CollectExpired",
  "0x96c65af3": "CollectModuleNotWhitelisted",
  "0xad4cf0a3": "CollectNotAllowed",
  "0x09b46ac0": "CommunityConditionNotWhitelisted",
  "0x1abd0d58": "EmergencyAdminCannotUnpause",
  "0xd325a6d6": "FollowInvalid",
  "0xefb6a45f": "FollowModuleNotWhitelisted",
  "0xb3c1497e": "FollowNotApproved",
  "0x2edfc66c": "HandleContainsInvalidCharacters",
  "0x5e58454e": "HandleFirstCharInvalid",
  "0x3eb64ab3": "HandleLengthInvalid",
  "0x902815b9": "HandleTaken",
  "0x48be0eb3": "InitParamsInvalid",
  "0x5daa87a0": "Initialized",
  "0x48e66ed5": "InvalidCommunityId",
  "0x613970e0": "InvalidParameter",
  "0x10ee8f55": "JoinModuleNotWhitelisted",
  "0xb9b82997": "LikeInvalid",
  "0xb643bfa6": "MintLimitExceeded",
  "0x46308bd5": "ModuleDataMismatch",
  "0x76166515": "NotCommunityOwner",
  "0x957fb99f": "NotDispatcher",
  "0xb56f932c": "NotGovernance",
  "0xa020ab32": "NotGovernanceOrEmergencyAdmin",
  "0xe14ddfc8": "NotHasProfile",
  "0xecbc82c0": "NotJoinCommunity",
  "0x55c12d52": "NotOSP",
  "0xdb1453ce": "NotOwnerOrApproved",
  "0xf194fae5": "NotProfileOwner",
  "0xf33ac98f": "NotProfileOwnerOrDispatcher",
  "0x7f591641": "NotSlotNFTOwner",
  "0x584a7938": "NotWhitelisted",
  "0x9e87fac8": "Paused",
  "0x561a8587": "ProfileCreatorNotWhitelisted",
  "0xa43d2a71": "PublicationDoesNotExist",
  "0x79c951d8": "PublishingPaused",
  "0xab7be9de": "ReferenceModuleNotWhitelisted",
  "0x547802a5": "SBTTokenAlreadyExists",
  "0xe405ba74": "SBTTransferNotAllowed",
  "0x0819bdcd": "SignatureExpired",
  "0x37e8456b": "SignatureInvalid",
  "0x0da293d1": "SlotNFTAlreadyUsed",
  "0x020f1a4c": "SlotNFTNotWhitelisted",
  "0x07431014": "TargetNotWhitelisted",
  "0xceea21b6": "TokenDoesNotExist",
  "0x1fac5b74": "ZeroSpender",
};

// TO-DO: define all res data type
export type OspRes = {
  data?: any;
  error?: any;
};
