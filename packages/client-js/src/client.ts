import { AbiItem } from "web3-utils";
import { Activity } from "./activity";
import { Auth } from "./auth";
import { Community } from "./community";
import { CHAIN_CONFIG } from "./constant";
import { IPFS } from "./ipfs";
import { NFT } from "./nft";
import { Profile } from "./profile";
import { Reaction } from "./reaction";
import { Relation } from "./relation";
import { Request } from "./request";
import { TypeData } from "./typedata";
import { Wallet } from "./wallet";
import { CoinMintPlugin } from "./plugin/coin-mint-plugin";
// require('./lib/metamask-sdk.js')

export type UR = Record<string, unknown>;
export type DefaultGenerics = {
  activityType: UR;
  childReactionType: UR;
  collectionType: UR;
  personalizationType: UR;
  reactionType: UR;
  userType: UR;
};

export type ClientOptions = {
  urlOverride?: Record<string, string>;
  timeout?: number;
  browser?: boolean;
  version?: string;
  cache?: boolean;
  cacheProvider?: any;
  env?: "dev" | "beta" | "local";
  error?: (error: Error) => void;
};

export type ContractConfig = {
  abi?: AbiItem[];
  gasPrice?: string;
};

export class Client<
  StreamFeedGenerics extends DefaultGenerics = DefaultGenerics
> {
  baseUrl: string;
  apiKey: string;
  appId?: string;
  apiSecret: string | null;
  userId?: string;
  version: string;
  options: ClientOptions;
  enableCache: boolean;
  isBrowser: boolean;
  // 不确定
  cache: any;
  wallet: Wallet;
  profile: Profile;
  community: Community;
  nft: NFT;
  address: string;
  request: Request;
  errorHandler?: (error: Error) => void;
  ipfs: IPFS;
  activity: Activity<StreamFeedGenerics>;
  relation: Relation<StreamFeedGenerics>;
  reaction: Reaction<StreamFeedGenerics>;
  auth: Auth<StreamFeedGenerics>;
  typedata: TypeData<StreamFeedGenerics>;
  coin_mint_plugin: CoinMintPlugin;

  // reactions: StreamReaction<StreamFeedGenerics>;
  constructor(
    apiKey: string,
    apiSecretOrToken: string | null,
    appId?: string,
    options: ClientOptions = {}
  ) {
    this.apiKey = apiKey;
    this.apiSecret = apiSecretOrToken;

    this.appId = appId;
    this.options = options;
    this.version = this.options.version || "v1.0";
    this.enableCache = this.options.cache || true;
    this.baseUrl = this.getBaseUrl();
    this.isBrowser =
      typeof location !== "undefined" && this.options.browser ? true : false;
    this.cache =
      typeof localStorage !== "undefined"
        ? localStorage
        : this.options.cacheProvider;
    if (options.error) {
      this.errorHandler = options.error;
    }
    this.request = new Request(this.baseUrl, this.errorHandler);

    this.wallet =
      typeof location !== "undefined" && options.browser
        ? new Wallet(this.request, CHAIN_CONFIG[options.env])
        : undefined;

    this.ipfs = new IPFS(this);
    this.nft = new NFT(this);
    this.activity = new Activity<StreamFeedGenerics>(this);
    this.relation = new Relation<StreamFeedGenerics>(this);
    this.profile = new Profile<StreamFeedGenerics>(this);
    this.community = new Community<StreamFeedGenerics>(this);
    this.reaction = new Reaction<StreamFeedGenerics>(this);
    this.auth = new Auth<StreamFeedGenerics>(this);
    this.typedata = new TypeData<StreamFeedGenerics>(this);
    this.coin_mint_plugin = new CoinMintPlugin(this);
  }

  getBaseUrl(serviceName?: string) {
    if (!serviceName) serviceName = "api";

    if (this.options.urlOverride && this.options.urlOverride[serviceName])
      return this.options.urlOverride[serviceName];

    return this.baseUrl;
  }
}
