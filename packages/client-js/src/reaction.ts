import getUuid from "uuid-by-string";
import { Client, DefaultGenerics } from "./client";
import { APPID, OnChainTypeEnum, SDK_ERROR } from "./constant";
import { getMainContentFocus } from "./util/getMainContentFocus";

import { Hex, hexToNumber } from "viem";
import {
  AddReactionRequest,
  CollectModule,
  CollectModuleEnum,
  CollectModuleParam,
  ContentTypeEnum,
  ReactionKindEnum,
  ReferenceModule,
  ReferenceModuleEnum,
  ReferenceModuleParam,
} from "./rest_api_generated";
import { ethers } from "ethers";
import { getEvent } from "./erc4337/receipt";

export type ReactionAddActivity = {
  target_user_id: string;
  target_activity_id: string;
  content_uri: string;
  reaction_kind: string;
  reference_module_param: ReferenceModuleParam;
};
export type ReactionAddOptions = {
  userId: string;
  collect: CollectModule;
  reference: ReferenceModule;
  collectParam: any;
  referenceParam: any;
  isOnChain: boolean;
};

export type ReactionCreateActivity = {
  target_user_id: string;
  target_activity_id: string;
  content_uri: string;
  reference_module_param: ReferenceModuleParam;
};
export type ReactionCreateOptions = {
  user_id: string;
  collect_module?: CollectModule;
  reference_module?: ReferenceModule;
  collect_module_param: CollectModuleParam;
  reference_module_param: ReferenceModuleParam;
};
const mockCollectModule = {
  type: CollectModuleEnum.FREE_COLLECT_MODULE,
  init: {
    FreeCollectModule: {
      only_follower: true,
    },
  },
};
const mocReferenceModule = {
  type: ReferenceModuleEnum.FOLLOWER_ONLY_REFERENCE_MODULE,
  init: {},
};

const mockCollectModuleParam = {
  type: CollectModuleEnum.FREE_COLLECT_MODULE,
  data: {},
};

const mocReferenceModuleParam = {
  type: ReferenceModuleEnum.FOLLOWER_ONLY_REFERENCE_MODULE,
  data: {},
};

export class Reaction<
  StreamFeedGenerics extends DefaultGenerics = DefaultGenerics
> {
  client: Client<StreamFeedGenerics>;
  constructor(client: Client<StreamFeedGenerics>) {
    this.client = client;
  }

  async add(
    kind: any,
    {
      content_id: referenced_content_id,
      view_id: referenced_view_id,
      profile_id: referenced_profile_id,
      isOnChain,
      metadata,
      reaction_kind,
      onChainType,
    }: any,
    params = {},
    onCompleted = (contentId: string) => {}
  ) {
    let content_uri = "";
    let _metadata;
    if (metadata) {
      _metadata = {
        ...{
          version: "2.0.0",
          metadata_id: getUuid(JSON.stringify(metadata)),
          external_url: `https://lenster.xyz/u/${this.client.profile.handle}`,
          image: null,
          imageMimeType: null,
          name: `Comment by ${this.client.profile.handle}`,
          tags: [],
          animation_url: null,
          mainContentFocus: getMainContentFocus(metadata),
          contentWarning: null,
          attributes: [
            {
              traitType: "type",
              displayType: "string",
              value: getMainContentFocus(metadata),
            },
          ],
          medias: [],
          locale: "zh-CN",
          appId: APPID,
        },
        ...metadata,
      };
      try {
        content_uri = await this.client.ipfs.upload(JSON.stringify(_metadata));
      } catch (e) {
        return { code: SDK_ERROR.IPFS_ERROR, ...e };
      }
    }

    if (isOnChain) {
      if (content_uri) {
        return await this.addContentWithChain(
          kind,
          {
            referenced_content_id,
            referenced_view_id,
            referenced_profile_id,
            content_uri,
            reaction_kind,
            metadata: _metadata,
            onChainType,
          },
          params,
          onCompleted
        );
      } else {
        return await this.addWithChain(
          kind,
          { referenced_content_id, referenced_view_id, referenced_profile_id },
          params,
          onCompleted
        );
      }
    } else {
      const res = await this.addRest(kind, {
        referenced_view_id,
        referenced_profile_id,
        content_uri,
        metadata: _metadata,
      });
      onCompleted(null);
      return res;
    }
  }

  async addContentWithChain(
    kind: ContentTypeEnum,
    {
      referenced_view_id,
      referenced_content_id,
      referenced_profile_id,
      metadata,
      content_uri,
      reaction_kind,
      onChainType,
    }: any,
    params: any,
    onCompleted = (contentId: string) => {}
  ) {
    let callData = this.client.wallet.ospClient.interface.encodeFunctionData(
      "addContentReaction",
      [
        {
          reactionType: 1,
          profileId: BigInt(hexToNumber(this.client.profile.profileId as Hex)),
          contentURI: content_uri,
          referencedProfileId: BigInt(
            hexToNumber(referenced_profile_id as Hex)
          ),
          referencedContentId: BigInt(
            hexToNumber(referenced_content_id as Hex)
          ),
          referenceModuleData: "0x",
          referenceModule: "0x0000000000000000000000000000000000000000",
          referenceModuleInitData: "0x",
        },
      ]
    ) as Hex;
    return this.client.wallet.account
      .sendOpAndGetResult({ data: callData }, (receipt) => {
        onCompleted(
          ethers.utils.hexValue(
            getEvent("ContentReactionCreated", receipt)["contentId"]
          )
        );
      })
      .then(async (res) => {
        if (res.error) {
          return { data: null, error: res.error };
        }
        if (onChainType === OnChainTypeEnum.ON_CHAIN) {
          const res2 = await this.addRest(reaction_kind, {
            referenced_view_id,
            referenced_profile_id,
            content_uri,
            metadata,
          });
          return { data: { ...res.data, ...res2.data }, error: null };
        }

        return { data: { ...res.data }, error: null };
      });
  }

  async addWithChain(
    kind: ReactionKindEnum | ContentTypeEnum,
    { referenced_content_id, referenced_view_id, referenced_profile_id }: any,
    params: any,
    onCompleted = (contentId: string) => {}
  ) {
    //目前只有点赞
    params.like.is_like;
    let callData = this.client.wallet.ospClient.interface.encodeFunctionData(
      "addReaction",
      [
        {
          reactionType: 0,
          profileId: BigInt(hexToNumber(this.client.profile.profileId as Hex)),
          referencedProfileId: BigInt(
            hexToNumber(referenced_profile_id as Hex)
          ),
          referencedContentId: BigInt(
            hexToNumber(referenced_content_id as Hex)
          ),
          referenceModuleData: "0x",
          data: ethers.utils.defaultAbiCoder.encode(
            ["bool"],
            [params.like?.is_like]
          ),
        },
      ]
    ) as Hex;
    return this.client.wallet.account.sendOpAndGetResult(
      { data: callData },
      (receipt) => {
        onCompleted(null);
      }
    );
  }

  /**
   * add reaction
   * @link https://getstream.io/activity-feeds/docs/node/reactions_introduction/?language=js#adding-reactions
   * @method add
   * @memberof StreamReaction.prototype
   * @param  {string}   kind  kind of reaction
   * @param  {string}   activity Activity or an ActivityID
   * @param  {ReactionType}   data  data related to reaction
   * @param  {ReactionAddOptions} [options]
   * @param  {string} [options.id] id associated with reaction
   * @param  {string[]} [options.targetFeeds] an array of feeds to which to send an activity with the reaction
   * @param  {string} [options.userId] useful for adding reaction with server token
   * @param  {object} [options.targetFeedsExtraData] extra data related to target feeds
   * @return {Promise<ReactionAPIResponse<ReactionType>>}
   * @example reactions.add("like", "0c7db91c-67f9-11e8-bcd9-fe00a9219401")
   * @example reactions.add("comment", "0c7db91c-67f9-11e8-bcd9-fe00a9219401", {"text": "love it!"},)
   */
  async addRest(
    kind: ReactionKindEnum,
    { referenced_view_id, content_uri, referenced_profile_id, metadata }: any
  ) {
    const body: AddReactionRequest = {
      kind,
      referenced_view_id,
      content_uri,
      referenced_profile_id,
      content:
        metadata && typeof metadata !== "string"
          ? JSON.stringify(metadata)
          : metadata,
    };

    const res = await this.client.request.call(
      ["reactions", "addReaction"],
      body
    );
    return res;
  }

  /**
   * get all reaction
   * @link https://getstream.io/activity-feeds/docs/node/reactions_introduction/?language=js#retrieving-reactions
   * @method get
   * @memberof StreamReaction.prototype
   * @param  {string}   id Reaction Id
   * @return {Promise<EnrichedReactionAPIResponse<StreamFeedGenerics>>}
   * @example reactions.get("ACTIVITY","67b3e3b5-b201-4697-96ac-482eb14f88ec","COMMENT", {limit:20})
   */
  async get(
    lookupAttr: string,
    lookupValue: string,
    kind: ReactionKindEnum,
    data: { ranking: any; limit: any; withActivity: any }
  ) {
    const query = {
      kind: kind,
      ranking: data?.ranking || "TIME",
      limit: data?.limit || 10,
      with_activity: data?.withActivity || false,
      with_child_count: true,
      ...data,
    } as any;

    const res = await this.client.request.call(
      ["reactions", "getReactions"],
      lookupAttr,
      lookupValue,
      kind,
      query
    );
    return res;
  }
  /**
   * get reaction detail
   * @link https://getstream.io/activity-feeds/docs/node/reactions_introduction/?language=js#retrieving-reactions
   * @method get
   * @memberof StreamReaction.prototype
   * @param  {string}   id Reaction Id
   * @return {Promise<EnrichedReactionAPIResponse<StreamFeedGenerics>>}
   * @example reactions.get("67b3e3b5-b201-4697-96ac-482eb14f88ec")
   */
  async getDetail(id: string) {
    const res = await this.client.request.call(
      ["reactions", "getReaction"],
      id
    );
    return res;
  }
  /**
   * delete reaction
   * @link https://getstream.io/activity-feeds/docs/node/reactions_introduction/?language=js#removing-reactions
   * @method delete
   * @memberof StreamReaction.prototype
   * @param  {string}   id Reaction Id
   * @return {Promise<APIResponse>}
   * @example reactions.delete("ACTIVITY","67b3e3b5-b201-4697-96ac-482eb14f88ec","COMMENT")
   */
  async deleteByKind(
    lookupAttr: string,
    lookupValue: string,
    kind: ReactionKindEnum
  ) {
    const res = await this.client.request.call(
      ["reactions", "deleteReactionByKind"],
      lookupAttr,
      lookupValue,
      kind
    );
    return res;
  }
}
