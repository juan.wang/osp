import {
  Client,
  DefaultGenerics,
  ClientOptions,
  ContractConfig,
} from "./client";

export function connect<
  StreamFeedGenerics extends DefaultGenerics = DefaultGenerics
>(
  apiKey: string,
  apiSecret: string | null,
  appId?: string,
  options?: ClientOptions
) {
  return new Client<StreamFeedGenerics>(apiKey, apiSecret, appId, options);
}
